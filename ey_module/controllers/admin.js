		var User = require('../mongo_handler/user.js')
		var Userpost = require('../mongo_handler/post.js')
		var path = require('path')
		var emailer = require('../global_vars/mailerfunction.js')
		var spark = require('../global_vars/sparkpost.js')
		var postcount = require('../mongo_handler/postcount.js')
		var Usercandidate = require('../mongo_handler/candidate.js')
		var industry = require('../mongo_handler/industry.js')
		var functionArea = require('../mongo_handler/functional.js')
		var mongoose = require("mongoose")
		var waterfall = require('async-waterfall');
		var xmlCsvParser = require('../global_vars/xmlcsvParser.js')
		var cloudinary = require('cloudinary');
		var interview = require('../mongo_handler/interview.js')
		var fs = require('fs')
		var async = require("async");
		var jwt = require('jsonwebtoken');
		var config = require('../global_vars/config');

		cloudinary.config({
		    cloud_name: 'dpijyjulg',
		    api_key: '344142488787362',
		    api_secret: 'E6textvz7MZkvU4H-zw4L2Ybxmw'
		});


		var admin_user = {

		    admin_login: function (req, res) {
		        console.log("req.body", req.body)
		        User.findOne({
		            email_id: req.body.email_id,
		            password: req.body.password,
		            user_type: "admin"
		        }, {
		            user_type: 1,
		            user_name: 1,
		            email_id: 1
		        }, function (err, result) {
		            if (err) {
		                res.send({
		                    responseMessage: "server err",
		                    responseCode: 500
		                })

		            } else if (!result) {
		                res.send({
		                    responseMessage: "Email or password is invalid.",
		                    responseCode: 204
		                })
		            } else {

		                var _token = jwt.sign({
		                    exp: Math.floor(Date.now() / 1000) + (60 * 60),
		                    data: req.body
		                }, config.authSecretKey);
		                res.send({
		                    responseCode: 200,
		                    responseMessage: "Login sucessfully",
		                    result: result,
		                    token: _token
		                })



		            }


		        })
		    },

		    admin_full_job_list: function (req, res) {
		        //        Userpost.find({}).sort({created_at:-1}).exec(function(err,result){
		        //       if(err){
		        //       	  res.send({
		        //      responseMessage :"server err",
		        //      responseCode :500
		        //   })
		        //
		        //       }
		        //       else if(result.length == 0){
		        //		  res.send({
		        //		      responseMessage :"No post found",
		        //		      responseCode :400
		        //		   })
		        //
		        //       }
		        //       else{
		        //	       	  res.send({
		        //	      responseMessage :"All post found",
		        //	      responseCode :200,
		        //	      result :result
		        //	   })
		        //
		        //       }
		        //
		        //        })
		        var query = [
//			    {
//			     $match :{client_id :mongoose.Types.ObjectId(req.params._id)}
//			        },
		            {
		                $unwind: "$candidates"
			    },
		            {
		                //               $match :{$and:[{'candidates.cv' : {$ne: null}},{'candidates.status' : "active"}]}
		                $match: {
		                    'candidates.cv': {
		                        $ne: null
		                    }
		                }

          },
		            {
		                $group: {
		                    _id: "$job_id",
		                    proposal: {
		                        $sum: 1
		                    }
		                }
			    }, {
		                $lookup: {
		                    from: "posts",
		                    as: "job_id",
		                    localField: "_id",
		                    foreignField: "_id"
		                }
			    },
		            {
		                $unwind: "$job_id"
			       },
		            {
		                $project: {
		                    proposal: 1,
		                    "job_id.job_title": 1,
		                    "job_id.company_name": 1,
		                    "job_id.deadline": 1,
		                    "job_id.ctc_max": 1,
		                    "job_id.status": 1,
		                    "job_id._id": 1
		                }
			       }

			   ]

		        Usercandidate.aggregate(query).exec(function (err, result) {

		            if (err) {
		                res.send({
		                    responseMessage: "server err",
		                    responseCode: 500
		                })

		            } else if (result.length == 0) {
		                res.send({
		                    responseMessage: "No post found",
		                    responseCode: 400
		                })

		            } else {
		                res.send({
		                    responseMessage: "ALl data found",
		                    responseCode: 200,
		                    result: result
		                })
		            }

		        })

		    },

		    admin_manage_user: function (req, res) {
		        User.find({
		            user_type: "1",
		            user_type: "0"
		        }, {
		            password: 0
		        }, function (err, result) {
		            if (err) {
		                res.send({
		                    responseMessage: "server err",
		                    responseCode: 500
		                })

		            } else if (result.length == 0) {
		                res.send({
		                    responseMessage: "No post found",
		                    responseCode: 400
		                })

		            } else {
		                res.send({
		                    responseMessage: "all user found",
		                    responseCode: 200,
		                    result: result
		                })

		            }


		        })

		    },


		    admin_latest_job_list: function (req, res) {
		        Userpost.find({}).sort({
		            'created_at': -1
		        }).limit(10).exec(function (err, result) {
		            if (err) {
		                res.send({
		                    responseMessage: "server err",
		                    responseCode: 500
		                })

		            } else if (result.length == 0) {
		                res.send({
		                    responseMessage: "No post found",
		                    responseCode: 400
		                })

		            } else {
		                res.send({
		                    responseMessage: "Client post found",
		                    responseCode: 200,
		                    result: result
		                })

		            }

		        })

		    },


		    admin_dasboard_count: function (req, res) {
		        async.parallel({
		            TotalClient: function (callback) {
		                User.count({
		                    user_type: "0"
		                }, function (err, result) {
		                    callback(null, result);
		                })

		            },

		            TotalRecruiter: function (callback) {
		                User.count({
		                    user_type: "1"
		                }, function (err, result) {
		                    callback(null, result);

		                })
		            },

		            TotalactiveClient: function (callback) {
		                User.count({
		                    status: "pending",
		                    "user_type": "0"
		                }, function (err, result) {
		                    callback(null, result)
		                })

		            },

		            TotalactiveRecruiter: function (callback) {
		                User.count({
		                    status: "pending",
		                    user_type: "1"
		                }, function (err, result) {
		                    callback(null, result)
		                })

		            },
		            TotalResumeSubmitted: function (callback) {
//		                callback(null, 0)
                        var query = [
//			    {
//			     $match :{client_id :mongoose.Types.ObjectId(req.params._id)}
//			        },
		            {
		                $unwind: "$candidates"
			    },
		            {
		                //               $match :{$and:[{'candidates.cv' : {$ne: null}},{'candidates.status' : "active"}]}
		                $match: {
		                    'candidates.cv': {
		                        $ne: null
		                    }
		                }

          },
		            {
		                $group: {
		                    _id: "$job_id",
		                    proposal: {
		                        $sum: 1
		                    }
		                }
			    }, 
		          {
		                $project: {
		                    proposal: 1
		                 
		                }
			       }

			   ]
                        Usercandidate.aggregate(query).exec(function(err, result){
                             callback(null, result) 
                            
                        })
		            }


		        }, function (err, results) {
		            console.log("results", results)

		            if (err) {
		                res.send({
		                    responseMessage: "server err",
		                    responseCode: 500


		                })
		            } else {
		                res.send({
		                    responseMessage: "Find All counts",
		                    responseCode: 200,
		                    result: results

		                })

		            }
		            //      err?res.status(500).send(err):res.send({result:results})
		            // results now equals to: [one: 'abc\n', two: 'xyz\n']
		        });

		    },

		    resdesk_admin: function (req, res) {
		        console.log("req.body----", req.params)
		        var query = [
		            {
		                $unwind: "$candidates"
                },
                     {
		                //               $match :{$and:[{'candidates.cv' : {$ne: null}},{'candidates.status' : "active"}]}
		                $match: {
		                    'candidates.cv': {
		                        $ne: null
		                    }
		                }

          },
                    
                    {
		                $lookup: {
		                    from: "posts",
		                    as: "job_id",
		                    localField: "job_id",
		                    foreignField: "_id"
		                }
                },
                {
                  $unwind :"$job_id"
                } ,
                {
		                $project: {
		                    candidates: 1,
		                    "job_id.job_title": 1,
		                    "job_id.industry": 1,
		                    "job_id.Location": 1,
		                    "job_id.experience": 1,
		                    "job_id.status": 1,
		                    "job_id._id": 1,
		                    "_id": 1
		                }
                }

            ]

		        Usercandidate.aggregate(query).exec(function (err, result) {
		            if (err) {
		                res.send({
		                    responseMessage: "server err",
		                    responseCode: 500

		                })

		            } else if (result.length == 0) {
		                res.send({
		                    responseMessage: "No result",
		                    responseCode: 400

		                })

		            } else {
		                res.send({
		                    responseMessage: "All cv found",
		                    responseCode: 200,
		                    result: result
		                })

		            }
		        })



		    },
   
   admin_view_resume : function(req,res){
   console.log("req----",req.body)

		var query = [

     {
     $match :{
      job_id :mongoose.Types.ObjectId(req.body.job_id)
     }
     },{
     $unwind :"$candidates"
     },{
      $lookup :{
                  from:"posts",
                    as:"job_id",
                    localField:"job_id",
                    foreignField:"_id"
      }
     },
   {
      $lookup :{
                  from:"users",
                    as:"recruiter_id",
                    localField:"recruiter_id",
                    foreignField:"_id"
      }
     },
     {
     $unwind :"$job_id"
     },
     {
         $unwind:"$recruiter_id"
     },
     {
     $project :{
        "job_id.job_title":1,
        "job_id._id":1,
        "candidates":1,
        "recruiter_id._id":1,
        "recruiter_id.user_name":1
     }
     }
		]

		Usercandidate.aggregate(query).exec(function(err,result){
			 if(err){
			 	  res.send({
		                    responseMessage: "server err",
		                    responseCode: 500

		                })

			 }
			 else{
			 	  res.send({
		                    responseMessage: "All cv found",
		                    responseCode: 200,
		                    result : result

		                })

			 }
		})


   },



            
        admin_cv_shortlist : function(req,res){
          console.log("req----",req.body)
          Usercandidate.findOneAndUpdate({_id:mongoose.Types.ObjectId(req.body._id),'candidates._id' :mongoose.Types.ObjectId(req.body.candidate_id)},{$set :{'candidates.$.ey_shortlist':true}},function(err,result){
              if(err){
                   res.send({
		                    responseMessage: "server err",
		                    responseCode: 500

		                })
                  
              }
              else{
                   res.send({
		                    responseMessage: "cv shortlisted suceesfully",
		                    responseCode: 200

		                })
                  
              }
              
          })
                
                
            }





		}

		module.exports = admin_user


		var get_count = function (callback) {
		    waterfall([
		        (cb) => {
		            postcount.findOne({}, function (err, success_data) {
		                console.log("success_data", success_data)
		                if (err) cb(err, null);
		                else cb(null, success_data);
		            })
		        },
		        (success_data, cb) => {
		            var v = success_data.postcount + 1
		            console.log("v-----", v)
		                // postcount.findByIdAndUpdate({_id:mongoose.Types.ObjectId(success_data._id)},{
		                //   $inc:postcount
		                // },{ projection: postcount , new :true},function(err,updated_count){
		                // 		console.log("updated_count",updated_count)
		                //   if(err) cb(err,null);
		                //   else cb(null,updated_count);
		                // })
		            postcount.findOneAndUpdate({
		                _id: mongoose.Types.ObjectId(success_data._id)
		            }, {
		                $set: {
		                    "postcount": success_data.postcount + 1
		                }
		            }, {
		                new: true
		            }, {
		                postcount: 1
		            }, function (err, updated_count) {
		                console.log("updated_count", updated_count)
		                if (err) cb(err, null);
		                else cb(null, updated_count);
		            })
		        }
		    ], function (err, result) {
		        if (err) {
		            console.log(err);
		        } else {
		            callback(null, result);
		        }
		    })
		}


		// var chkemail = function (email){
		//       return new Promise(data){
		//       	var c = User.findOne({email_id:req.body.email_id},function(err,result){
		//       		if(resolve){
		//       		 resolve(result)
		//       		}
		//       		else{
		//       			 reject(err)
		//       		}
		//       	})
		//       }

		//      } 

		//      var checkPassword  = function (result, password){
		//          return new Promise(data,password){
		//          	if(data.password == req.body.password){
		//          	var d = User.findOne({email_id:result.email_id, password : result.password},function(err, result){
		//          		if(resolve){
		//                        resolve(result)
		//          		}
		//          		else{
		//                     reject(err)
		//          		}
		//          	})
		//          }
		//          else{
		//          	 resolve({"msg" :"please enter correct password"})
		//          }
		//          }

		//      }