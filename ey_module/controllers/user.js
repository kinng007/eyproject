var User = require('../mongo_handler/user.js')
var path = require('path')
var emailer = require('../global_vars/mailerfunction.js')
var spark = require('../global_vars/sparkpost.js')
var mongoose = require("mongoose")
var jwt = require('jsonwebtoken');
var config=require('../global_vars/config');
    var waterfall = require('async-waterfall');
    var cloudinary = require('cloudinary');
        var fs = require('fs')
    var async=require("async");

 cloudinary.config({ 
    cloud_name: 'dpijyjulg', 
  api_key: '344142488787362', 
  api_secret: 'E6textvz7MZkvU4H-zw4L2Ybxmw' 
});

var user_api = {


first_signup : function(req,res){
  console.log("req---",req.body)
  User.findOne({email_id:req.body.email_id},function(err,result){
   if(err){
   	res.send({
   		responseMessage :"server err",
   		responseCode :500
   })
   }
   else if(!result){
   	var user_save  = new User(req.body)
   	  user_save.save(function(err,result){
    var htmlContent=spark.getVerificationMailHtml(result.user_name,result.email_id,path)
    spark.send('noreply@peopleinfinia.com',req.body.email_id,'Account verificaion',htmlContent).then(function(result1) {
      console.log("result1----",result1)
     if(err){
     	res.send({
   		responseMessage :"err",
   		responseCode :500
   })

     }
     else{
     	res.send({
   		responseMessage :"Please check Your mail and activate the link.",
   		responseCode :200
   })
     }

   	  })
  })

   }
   else{
   	 	res.send({
   		responseMessage :"This Email Id is already exists",
   		responseCode :400
   })
    
   }
  })

},


forget_password : function(req,res){
  console.log("req---",req.body)
  User.findOne({email_id:req.body.email_id},function(err,result){
   if(err){
     return res.send({responseCode:500,responseMessage:"server err"})
   }
   else if(!result){
                return res.send({responseCode:400,responseMessage:"Please Enter correct email ID"})
   }
   else{
    function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
    var rString = randomString(8, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
     
      // var htmlContent=mailTemp.style+mailTemp.content_start1+functions.getforgot(req.body.email,rString)+mailTemp.content_end4;
      var htmlContent=spark.forgetMailHtml(result.user_name,result.email_id,rString)
       spark.send('noreply@peopleinfinia.com', req.body.email_id, 'Forgot password',htmlContent ).then(function(result) {
        if(!result.status) { return res.send({responseCode:400,responseMessage:"mail Not sent!"}) }
                else{
                  User.update({email_id:req.body.email_id},{$set:{password:rString}},function(err,result2){
                     // res.json({ status: true, response : 'please check your mail inbox.'})
                      return res.send({responseCode:200,responseMessage:"please check your mail inbox."})
                  })
                
                }
            })
  

   }

  })

},

// test
get_profile : function(req,res){
    console.log("enter the data ")
  User.findOne({_id:req.params._id},{password:0},function(err,result){
    res.send({
      responseMessage :"Profile found",
      responseCode :200,
      result:result
   })

  })

},
 

account_verification : function(req,res){
  console.log("req.params",req.params.email_id)
  User.findOne({email_id:req.params.email_id},function(err,result){
   if(err){
      res.send({
      responseMessage :"server err",
      responseCode :500
   })

   }
   else if(result.is_emailVerify == true){
      res.send({
      responseMessage :"Already Verified!!!",
      responseCode :204
   })

   }
   else{
    User.update({email_id:req.params.email_id},{$set:{is_emailVerify:true}},function(err,finalResult){
    res.redirect("/");

    })
 
   }


  })

},

login: function(req,res){
  console.log("req.body",req.body)
   User.findOne({email_id:req.body.email_id,password:req.body.password,is_emailVerify:true},{user_type:1,user_name:1},function(err,result){
   if(err){
    res.send({
      responseMessage :"server err",
      responseCode :500
   })

   }
   else if(!result){
    res.send({
      responseMessage :"Email or password is invalid.",
      responseCode :204
   })
   }
   else{
         User.update({_id:mongoose.Types.ObjectId(result._id)},{$set:{is_first_login : true }}, function(err,result3){
      var _token=jwt.sign({
                      exp: Math.floor(Date.now() / 1000) + (60 * 60),
                      data: req.body
                    }, config.authSecretKey);
                    res.send({responseCode:200,responseMessage :"Login sucessfully",result:result,token:_token})

      })

   }


   })


},

edit_profile : function(req,res){
  console.log("reqq---",req.body)
  console.log("reqq-----",req.params)
  waterfall([
               function(cb){
                        if(req.body.profile_pic){
                   var img_base64 = req.body.profile_pic;
                   binaryData = new Buffer(img_base64, 'base64');
                   require("fs").writeFile("profile.jpeg", binaryData, "binary", function (err) {
                       console.log(err);
                   });   
                                cloudinary.uploader.upload("profile.jpeg", function (result) {
                                    req.body.profile_pic=result.url;                            
                                    cb(null,result);                        
                           })           
                    }   else{
                       console.log("enter in pdf else");
                         req.body.profile_pic= "https://i.stack.imgur.com/l60Hf.png";
                        cb(null,'done');
                  }

               },
               function(result,cb){
                User.findOneAndUpdate({_id:mongoose.Types.ObjectId(req.params._id)},{$set:req.body},function(err,finalResult){
                 if(err){
                     cb(null)
                 }
                 else{
                  cb(null, finalResult)
                 }

                })
          
               }

    ],function(err,result){
    if(err){
       res.send({
         responseMessage :"server err",
           responseCode :500
       })
    }
    else{
      res.send({
           responseMessage :"Done sucessfully",
           responseCode :200
      })
    }
    })
},


change_password : function(req,res){
  console.log("req---",req.body)
  User.findOne({_id : mongoose.Types.ObjectId(req.params._id)},function(err,result){
   if(err){
     res.send({
      responseMessage :"server err",
      responseCode :500
   })

   }
   else if(result.password != req.body.oldPassword){
     res.send({
      responseMessage :"Please Enter correct old password",
      responseCode :204
   })

   }
   else {
    User.update({_id : mongoose.Types.ObjectId(result._id)},{$set :{password:req.body.newPassword}},function(err,result2){
     if(err){
      res.send({
      responseMessage :"server err",
      responseCode :500
   })

     }
     else{
      res.send({
      responseMessage :"password change sucessfully",
      responseCode :200
   })

     }

    })

   }

  })


}



}

module.exports = user_api 



