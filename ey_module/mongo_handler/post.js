var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postSchema = new Schema({

	job_id:{
		type:Number
	},
  user_id:{
   type: mongoose.Schema.Types.ObjectId,ref: 'user'
  },
  job_title :{
  	type:String
  },
  industry :{
  	type:String
  },
  functional_area :{

  	type:String
  },
  skills :[],
bookmarks:[{type:mongoose.Schema.Types.ObjectId,ref: 'user'}],
  vacancy :{
  	type:Number
  },
   save_draft:{
   	type:Boolean
   },
   duplicate_post :{
   	type:Boolean
   },

  Location :{
  	type:String
  },
  qulification :{
  	type:String
  },
  experience:{
  	type:String
  },
  description :{
  	type:String
  },
  company_name:{
    type:String
  },
  ctc_min:{
  	type:String
  },
  ctc_max:{
    type:String
  },
  perks :{
   type:String
  },
  commission :{
  	type:Number
  },
doc :{
  type:String
},
deadline:{
	type:String
},
notice_period:{
	type:String
},
contact_period:{
	type:String
},

  status:{
  	type:String,
    default :"active"
  },

  created_at:{
  	type:Date,
  	default :Date.now()
  }





});

var post=mongoose.model('post',postSchema)

module.exports = post; 



