var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var candidateSchema = new Schema({

job_id:{
type: mongoose.Schema.Types.ObjectId,ref: 'posts'
},
recruiter_id:{
  type: mongoose.Schema.Types.ObjectId,ref: 'users'
},
client_id:{
  type: mongoose.Schema.Types.ObjectId,ref: 'users'
},
candidates:[{
  name:{
    type:String
  },
age :{
  type:Number
}, 
  sex :{
    type:String
  },
  total_experience :{
    type :String
  },
  email:{
    type:String
  },
  experience:{
    type:String
  },
  qualification:{
    type:String
  },
  skill:[],
  phone_number:{
    type:Number
  },
  cv:{
    type:String,
    default :null
  },
  ctc :{
   type:String
  },
  current_organisation :{
    type:String
  },
  background_check :{
    type:Boolean,
    default :false
  },
  status:{
    type:String,
    status:"active"
  },
  withdraw : {
    type:Boolean
  },
    ey_shortlist :{
        type:Boolean,
        default : false
    }
}],
  status:{
  	type:String
  },
  created_at:{
  	type:Date,
  	default :Date.now()
  }
});

var candidate=mongoose.model('candidate',candidateSchema)

module.exports = candidate; 



