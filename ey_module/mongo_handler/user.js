var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({

     user_type:{     // 0 for client 1 for consoultant  // "admin for admin"
       type:String
     },
	user_name :{
		type:String
	},
	email_id:{
		type:String
	},
	password:{
		type:String
	},
	mobile:{
    type:Number
	},

	company_name:{
		 type:String
	},
	profile_pic : {
          type:String
	},
   bookmarks:[

   {
   	type:mongoose.Schema.Types.ObjectId,ref: 'post'
   }
   ],

		type:{    // consultant type
		  type:String
		},  
		 experience :{
		 	type :String
		 },
		 naukri_portal_login :{
		 	 type:Boolean
		 },
		 city :{
		 	type:String
		 },
		recruiter_working_type :{
			type:String
		},
		working_package :{
			type:String
		},
		functional_expert :{
			type:[]
		},
		industry_expert :{
			type :[]
		},
		ctc_worked :{
			type:String
		},
		commision :{
			type:String
		},
		pan_number :{
			type :String
		},
		gst_number :{
			type:String
		},
		


	status :{
		type:String,
		default:"pending"
	},
	is_login :{ // true or false
     type:Boolean,
     default:false
	},
    is_first_login :{ // true or false
          type:Boolean,
          default : false
    },
	is_emailVerify:{  // true or false
		type:Boolean,
		default:false
	},
	is_otpVerify :{  // true or false
		type:Boolean,
		default:false
	},
	created_at:{
		type:Date,
		default : Date.now()
	}


});

var user=mongoose.model('user',userSchema)

module.exports = user; 



