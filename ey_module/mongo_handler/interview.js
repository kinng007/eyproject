var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var interviewSchema = new Schema({

  job_id :{
    type:mongoose.Schema.Types.ObjectId,ref: 'post'
  },

  recruiter_id :{
    type:mongoose.Schema.Types.ObjectId,ref: 'user'
  },
  client_id :{
     type:mongoose.Schema.Types.ObjectId,ref: 'user'
  },

  candidate_id :{
     type:mongoose.Schema.Types.ObjectId, ref : 'candidate'
  },
  candidate_name :{
    type:String
  },

  location :{
   type:String
  },
  interview_time :{
    type:String
  },
  interview_date:{
   type:String
  },
  interview_type :{
    type:String
  },
  interview_person :{
    type:String
  },
  status :{   // 0 scheduled // 1 rescheduled // 2 empty
    type:String,
    default :"0"
  },
  created_at:{
  	type:Date,
  	default :Date.now()
  }

});

var interview=mongoose.model('interview',interviewSchema)

module.exports = interview; 



