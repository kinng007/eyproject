var SparkPost = require('sparkpost');
var config = require('./config.js')
var client = new SparkPost(config.sparkPostApiKey);

module.exports = {
	send : function(from_, to_, subject_, message_) {
		  	// from_="no-reply@senzecit.com"
		  	console.log("from_---",from_)
		  	  	console.log("subject_---",subject_)
		  	  	  	console.log("message_---",message_)

		return new Promise(function(resolve, reject) {
			
			client.transmissions.send({
		    options: {
		      sandbox: false
		    },
		    content: {
		      from: from_,
		      subject: subject_,
		      html: message_
		 
		    },
		    recipients: [
		      {address: to_}
		    ]
		  	})
		  	.then(data => {
		  		console.log("data",data)
			    resolve({status: true})
		  	})
		  	.catch(err => {
		  		console.log("err----",err)
		  		resolve({status: false})
		  	});

		});
	},

	 getVerificationMailHtml: function(firstName,email_id){
		return '<div style="margin: 25px;"><p>Dear '+firstName+',<br><br>It is my pleasure to Welcome you to People Infinia, Indias first of  its kind AI enabled market place for consultants and one stop shop for all your talent need. Looking forward to fruitful association . Please, click on the link provided below to complete your email verification.</p><h2><a href="http://162.241.201.48:80/user/account_verification/'+email_id+'">Click Here to Activate Account</a></h2><div>Let’s get started with these simple steps:<br><ul><li>Post your requirement</li><li>Shortlist consultants basis the proposals received</li><li>Begin your engagement</li></ul></div></p></div>';
	},

		forgetMailHtml: function(firstName,email_id,password){
		return '<div style="margin: 25px;"><p>Dear '+firstName+',<br><br><p> Thanks for using People Infinia Here is the password of your account <h1>'+password+'</h1><br><br>Thanks, and Stay Blessed!<br><br>-Team People Infinia';
	},


	clientPostHtml : function(firstName,email_id,job_title,vacancy){
   return '<div style="margin: 25px;"><p>Hey,<br><br><p> Client is posted a Job Detail for <b>'+ job_title +'</b> and required vacancy is <b>'+ vacancy +' </b> Please check Full details on People Infinia Admin <br><br>Thanks, and Stay Blessed!<br><br>-Team People Infinia';
	}

}

