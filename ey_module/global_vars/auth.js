var jwt = require("jsonwebtoken");
var config = require("./config")();

module.exports.authUser = function (req, res, next) {
    var token = req.headers.token;
    if (token) {
        jwt.verify(token, config.authSecretKey, function (err, decoded) {
            
            if (err) {
                return res.json({
                    success: false
                    , message: 'Failed to authenticate token.'
                });
            }
            else {
                req.decoded = decoded;
                console.log("decode---"+req.decode);
                console.log(decoded);
                       next();
                   
                  
             
            }
        });
    }
    else {
        return res.status(403).send({
            success: false
            , message: 'No token provided.'
        });
    }
}
