import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.component.service'

@Component({
  selector: 'app-all-send-proposal',
  templateUrl: './all-send-proposal.component.html',
  styleUrls: ['./all-send-proposal.component.css']
})
export class AllSendProposalComponent implements OnInit {

	    public userId:any;
	    recruiterJobs:any=[];
  constructor(private appService:AppService) {
    this.userId = localStorage.getItem('loginSessId');
  }


  ngOnInit() {
    this.getList();
  }
  
  getList(){
   this.appService.recruiterAllJobList()
   .subscribe(
        res => {
          //var result =JSON.parse(res);
          this.recruiterJobs=res.result;
          console.log(this.recruiterJobs)
          // alert(JSON.stringify(res));
          return res;
        },
        err => {
          console.log("Error occured");
          return err;
        }
      );

  }

}
