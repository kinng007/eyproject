import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { Ng2CompleterModule } from "ng2-completer";

import { Ng5SliderModule } from 'ng5-slider';

import { AccordionModule } from 'ngx-bootstrap/accordion';
// import {
//   SocialLoginModule, 
//   AuthServiceConfig,
//   GoogleLoginProvider, 
//   FacebookLoginProvider, 
//   LinkedinLoginProvider,

// } from 'ng4-social-login';
import { Component, OnInit, Output, EventEmitter,NgModule,Injectable} from '@angular/core';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

import { BarRatingModule } from "ngx-bar-rating";

import { TagInputModule } from 'ngx-chips';



import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RecruiterDashboardComponent } from './recruiter-dashboard/recruiter-dashboard.component';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { ClientHeaderComponent } from './client-header/client-header.component';
import { RecruiterHeaderComponent } from './recruiter-header/recruiter-header.component';
import { ManageJobsComponent } from './manage-jobs/manage-jobs.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { SupportIssueComponent } from './support-issue/support-issue.component';
import { ScheduleComponent } from './schedule/schedule.component';
import swal from 'sweetalert';
import { PostNewJobComponent } from './post-new-job/post-new-job.component';
import { ReqJobProposalComponent } from './req-job-proposal/req-job-proposal.component';
import { ViewResponseComponent } from './view-response/view-response.component';
import { AllSendProposalComponent } from './all-send-proposal/all-send-proposal.component';
import { RecruiterProfileComponent } from './recruiter-profile/recruiter-profile.component';
import { ViewProposalComponent } from './view-proposal/view-proposal.component';
import { CandidateRecuiterComponent } from './candidate-recuiter/candidate-recuiter.component';
import { RecuiterManageJobsComponent } from './recuiter-manage-jobs/recuiter-manage-jobs.component';
import { RecruiterScheduleComponent } from './recruiter-schedule/recruiter-schedule.component';
import { ViewResumeComponent } from './view-resume/view-resume.component';
import { JobStatsComponent } from './job-stats/job-stats.component';
import { LoginComponent } from './login/login.component';
import { AboutComponent } from './about/about.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { FaqComponent } from './faq/faq.component';
import { RecruiterSignUpComponent } from './recruiter-sign-up/recruiter-sign-up.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeService } from './home/home.component.service';
import { LoginService } from './login/login.service';
import { HttpModule,Http } from '@angular/http'; 
import {NgxPaginationModule} from 'ngx-pagination';
import { CommonFunctionsService} from './sheared';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard, AdminAuthGuard } from './guards/index';
import { AuthInterceptor } from './HttpIntercepter';
import { AppService } from './app.component.service'
//import { Ng5SliderModule } from 'ng5-slider';
import { MyDatePickerModule } from 'mydatepicker';
import { TermsComponent } from './terms/terms.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

import 'flatpickr/dist/flatpickr.css';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
 import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ContactUsComponent } from './contact-us/contact-us.component';

import { ToastrModule } from 'ngx-toastr';
import { NewComponentComponent } from './new-component/new-component.component';
import { EditJobsComponent } from './edit-jobs/edit-jobs.component';

//admin
import { HeaderComponent } from './admin/header/header.component';
import { OverviewComponent } from './admin/overview/overview.component';
import { LeadsComponent } from './admin/leads/leads.component';
import { AdminManageJobsComponent } from './admin/manage-jobs/admin-manage-jobs.component';
import { ManageUserComponent } from './admin/manage-user/manage-user.component';
import { IssueComponent } from './admin/issue/issue.component';
import { ResdeckComponent } from './admin/resdeck/resdeck.component';
import { ProfileReportComponent } from './admin/profile-report/profile-report.component';
import { ResumePdfComponent } from './admin/resume-pdf/resume-pdf.component';
import { ViewStatusComponent } from './admin/view-status/view-status.component';
import { UserProfileComponent } from './admin/user-profile/user-profile.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { ViewResumeAdminComponent } from './admin/view-resume-admin/view-resume-admin.component';


import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ClientprofileComponent } from './clientprofile/clientprofile.component';



// const CONFIG = new AuthServiceConfig([
//   {
//     id: LinkedinLoginProvider.PROVIDER_ID,
//     provider: new LinkedinLoginProvider('81r3ougl97r3pk')
//   }
// ],null);
 
// export function provideConfig() {
//   return CONFIG;
// }

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RecruiterDashboardComponent,
    ClientDashboardComponent,
    ClientHeaderComponent,
    RecruiterHeaderComponent,
    ManageJobsComponent,
    CandidatesComponent,
    SupportIssueComponent,
    
    ScheduleComponent,
    PostNewJobComponent,
    ReqJobProposalComponent,
    ViewResponseComponent,
    AllSendProposalComponent,
    RecruiterProfileComponent,
    ViewProposalComponent,
    CandidateRecuiterComponent,
    RecuiterManageJobsComponent,
    RecruiterScheduleComponent,
    ViewResumeComponent,
    JobStatsComponent,
    AboutComponent,
    ContactUsComponent,
    MainHeaderComponent,
    FaqComponent,
    RecruiterSignUpComponent,
    LoginComponent,
    TermsComponent,
    PrivacyPolicyComponent,
    ContactUsComponent,
    NewComponentComponent,
    EditJobsComponent,
    //admin
    HeaderComponent,
    AdminLoginComponent,
    OverviewComponent,
    LeadsComponent,
    AdminManageJobsComponent,
    ManageUserComponent,
    IssueComponent,
    ResdeckComponent,
    ProfileReportComponent,
    ResumePdfComponent,
    ViewStatusComponent,
    UserProfileComponent,
    ClientprofileComponent,
    ViewResumeAdminComponent,
    
  ],
  imports: [
  TagInputModule,
    BrowserModule,
    //SocialLoginModule,
    AngularDateTimePickerModule,
    TagInputModule,
    NgxPaginationModule,
    CarouselModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    OwlDateTimeModule, 
    PdfViewerModule,
NgxChartsModule,
    Ng2CompleterModule,
    Ng5SliderModule,
    OwlNativeDateTimeModule,
    BarRatingModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,

FlatpickrModule.forRoot(),
    NgbModalModule.forRoot(),
        CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    MyDatePickerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),


    RouterModule.forRoot([
      {path: 'login', component: AdminLoginComponent},
      {
        path: 'admin-home', component: HeaderComponent, children: [
          { path: 'overview', component: OverviewComponent },
          { path: 'manageUser', component: ManageUserComponent },
          { path: 'leads', component: LeadsComponent },
          { path: 'manageJobs', component: AdminManageJobsComponent },
          { path: 'issue', component: IssueComponent },
          { path: 'resdesk', component: ResdeckComponent },
          { path: 'profilereport', component: ProfileReportComponent },
          { path: 'resumepdf', component: ResumePdfComponent },
          { path: 'viewstatus', component: ViewStatusComponent },
          { path: 'userprofile', component: UserProfileComponent },
          { path: 'viewResume/:id', component: ViewResumeAdminComponent }

        ]
      },
      {path: 'recruiterSignUp', component: RecruiterSignUpComponent},
      {path: 'viewResume/:id', component: ViewResumeComponent},
      {path: 'main', component: MainHeaderComponent,children:[
          {path: 'home', component: HomeComponent},
          {path: 'about', component: AboutComponent},
          {path:'terms',component:TermsComponent},
          {path:'contactus',component:ContactUsComponent},
          {path:'privacy-policy',component:PrivacyPolicyComponent},
          {path: 'faq', component: FaqComponent},
          {path: 'login', component: LoginComponent}
           ]},
      {path: 'clientHeader', component: ClientHeaderComponent,
        children: [
           {path: 'schedule/:jobId/:id', component: ScheduleComponent},
           {path: 'clientDashboard', component: ClientDashboardComponent},
           {path: 'manageJobs', component: ManageJobsComponent},
           {path: 'candidates', component: CandidatesComponent},
           {path: 'supportIssue', component: SupportIssueComponent},
           {path: 'profile', component: ClientprofileComponent},
           {path: 'schedule', component: ScheduleComponent},
           {path: 'postNewJob', component: PostNewJobComponent},
           {path: 'ReqJobProposal', component: ReqJobProposalComponent},
           {path: 'viewResponse/:id', component: ViewResponseComponent},
           {path: 'edit-jobs/:jobId', component: EditJobsComponent},
          ]
        },
      {path: 'recruiterHeader', component: RecruiterHeaderComponent,
        children: [
          {path: 'recuiterDashboard', component: RecruiterDashboardComponent},
           {path: 'sendProposal', component: AllSendProposalComponent},
           {path: 'recruiterProfile', component: RecruiterProfileComponent},
           {path: 'ViewProposal/:id', component: ViewProposalComponent},
           {path: 'candidateRecuiter', component: CandidateRecuiterComponent},
           {path: 'recuiterManageJobs', component: RecuiterManageJobsComponent},
           {path: 'supportIssue', component: SupportIssueComponent},
           {path: 'recruiterSchedule', component: RecruiterScheduleComponent},
           {path: 'jobStats/:id', component: JobStatsComponent}
        ]
      },
      { path: '',   redirectTo: '/main/home', pathMatch: 'full' },
      { path: 'admin',   redirectTo: '/login', pathMatch: 'full' }
    ])
  ],
  exports:[
    ScheduleComponent
    ],
  providers:[AuthInterceptor,{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      //useFactory: provideConfig,
      multi: true,
    },
    AuthGuard,
    AdminAuthGuard,
    //{
   //   provide: AuthServiceConfig,
   //   useFactory: provideConfig,
   // },
    HomeService,
    LoginService,
        CommonFunctionsService,
    AppService
   
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
