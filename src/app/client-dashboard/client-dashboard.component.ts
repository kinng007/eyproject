import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AppService } from '../app.component.service'

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.css']
})
export class ClientDashboardComponent implements OnInit {
   clientCount:any=[];
   jobFeeds=[];
   eyCount;
   intervieweS;
   userD;
   noInterviews = false;
   noFeeds = false ;
   name = "";
   public userId:any;
   ngOnInit(){
    this.getSheduledInterview();
   }
public constructor(private appService:AppService,private router: Router,appService1:AppService) {
     this.userId = localStorage.getItem('loginSessId');

this.appService.clientDashboardCount(JSON.parse(this.userId))
    .subscribe(
        res => {
          //var result =JSON.parse(res);
          this.clientCount= res.result;
          this.eyCount = this.clientCount.ey_qualifiedResume[0].count;
          //alert(JSON.stringify(res));
          console.log("res---",res)
          return res;
        },
        err => {
          console.log("Error occured");
          return err;
        }
    );
    
    this.appService.clientDashboardFeed(JSON.parse(this.userId))
    .subscribe(
        res => {
          //var result =JSON.parse(res);
          if(res.responseCode === 204){
            this.noFeeds = true;
          }
          this.jobFeeds = res.result;
          //alert(JSON.stringify(res));
          console.log(this.noFeeds)

          console.log("res56565---",res)
          return res;
        },
        err => {
          console.log("Error occured");
          return err;
        }
    );
  this.appService.recruiterProfile(JSON.parse(this.userId)).subscribe(objS=>{
           var resp = objS.json();
           this.userD=resp.result;
           this.name = this.userD.user_name
           console.log(this.userD);
        },objE=>{
          console.log(objE)
         
        })
}
  formVal = 1;

 

  
  
get user(): any {
    var user = localStorage.getItem('loginSessId');
    console.log(user)
    if(user){
      user = JSON.parse(user)
      return user
    }
   
  }


getSheduledInterview(){
  this.appService.getScheduledInterview(JSON.parse(this.userId)).subscribe((data)=>{
    console.log(data,"interviewed list")
     if(data.responseCode === 204){
            this.noInterviews = true;
          }
    this.intervieweS = data.result;
          console.log(this.noInterviews)

    },(err)=>{
      console.log(err);
    }
  )}
}
