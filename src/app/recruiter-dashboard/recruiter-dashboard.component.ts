import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.component.service'
import { PostjobService } from '../post-new-job/postjob.service'

@Component({
  selector: 'app-recruiter-dashboard',
  templateUrl: './recruiter-dashboard.component.html',
  styleUrls: ['./recruiter-dashboard.component.css']
})
export class RecruiterDashboardComponent implements OnInit {
  dashboardJobs:any=[];
  dashboardCount:any=[];
  feeds:any=[];
  user;
  name = "";
  Totalshortlist=0;
  eyCount;
  TotalPlaced=0;
  public userId:any;

  constructor(
    private appService:PostjobService,
    private appService1:AppService ) { 
     this.userId = localStorage.getItem('loginSessId');
  }
 

  ngOnInit() {
    this.getUser(this.userId);
    this.getDashboardJobs();
    this.getRecruiterDasboard(this.userId);
    this.getNotificationFeeds(this.userId);
  }
 
  getUser(id){
  this.appService1.recruiterProfile(JSON.parse(id)).subscribe(objS=>{
           var resp = objS.json();
           this.user=resp.result;
           this.name = this.user.user_name
           console.log(this.user);
        },objE=>{
          console.log(objE)
         
        })
      }

  

  saveBookMark(jobId){
    this.appService1.saveBookMark(JSON.parse(this.userId),jobId)
    .subscribe(
        res => {
          this.dashboardCount=res.result;

          console.log((JSON.stringify(res)));
          return res;
        },  err => {
          console.log("Error occured");
          return err;
        }
    );
  }
 
getNotificationFeeds(id){
  this.appService1.getNotificationsFeedRecuiterDashboard(JSON.parse(id)).subscribe((data)=>{
    console.log(data);
    this.feeds = data.result;
  },(error)=>{
    console.log(error);
  })
}
  getDashboardJobs(){
    this.appService.dashboardJob().subscribe(res => {
          this.dashboardJobs=res.result;
          console.log(this.dashboardJobs);
          return res;
        },err => {
          console.log("Error occured");
          return err;
        }
      );
  }
  getRecruiterDasboard(id){
    this.appService1.recruiterDashboardCount(JSON.parse(id)).subscribe(res => {
          this.dashboardCount=res.result;
           this.eyCount = this.dashboardCount.Eyqualified[0].count;
          this.Totalshortlist = this.dashboardCount.Totalshortlist[0].count;
          this.TotalPlaced = this.dashboardCount.TotalPlaced[0].count;

          console.log(this.dashboardCount);
          return res;
        },err => {
          console.log("Error occured");
          return err;
        }
    );
  }
}
