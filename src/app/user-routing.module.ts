import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './home/home.component';
import { RecruiterDashboardComponent } from './recruiter-dashboard/recruiter-dashboard.component';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { ClientHeaderComponent } from './client-header/client-header.component';
import { RecruiterHeaderComponent } from './recruiter-header/recruiter-header.component';
import { ManageJobsComponent } from './manage-jobs/manage-jobs.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { SupportIssueComponent } from './support-issue/support-issue.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { PostNewJobComponent } from './post-new-job/post-new-job.component';
import { ReqJobProposalComponent } from './req-job-proposal/req-job-proposal.component';
import { ViewResponseComponent } from './view-response/view-response.component';
import { AllSendProposalComponent } from './all-send-proposal/all-send-proposal.component';
import { RecruiterProfileComponent } from './recruiter-profile/recruiter-profile.component';
import { ViewProposalComponent } from './view-proposal/view-proposal.component';
import { CandidateRecuiterComponent } from './candidate-recuiter/candidate-recuiter.component';
import { RecuiterManageJobsComponent } from './recuiter-manage-jobs/recuiter-manage-jobs.component';
import { RecruiterScheduleComponent } from './recruiter-schedule/recruiter-schedule.component';
import { ViewResumeComponent } from './view-resume/view-resume.component';
import { JobStatsComponent } from './job-stats/job-stats.component';
import { AboutComponent } from './about/about.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { FaqComponent } from './faq/faq.component';
import { RecruiterSignUpComponent } from './recruiter-sign-up/recruiter-sign-up.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
      {path: 'main', component: MainHeaderComponent,children:[
          {path: 'home', component: HomeComponent},
          {path: 'about', component: AboutComponent},
          {path:'terms',component:TermsComponent},
          {path:'contactus',component:ContactUsComponent},
          {path:'privacy-policy',component:PrivacyPolicyComponent},
          {path: 'faq', component: FaqComponent},
          {path: 'login', component: LoginComponent}
      ]},
      
          {path: 'recruiterSignUp', component: RecruiterSignUpComponent},
      {path: 'viewResume/:id', component: ViewResumeComponent},
      {path: 'clientHeader', component: ClientHeaderComponent,
        children: [
           {path: 'schedule/:jobId/:id', component: ScheduleComponent},
            {path: 'clientDashboard', component: ClientDashboardComponent},
           {path: 'manageJobs', component: ManageJobsComponent},
           {path: 'candidates', component: CandidatesComponent},
           {path: 'supportIssue', component: SupportIssueComponent},
           {path: 'schedule', component: ScheduleComponent},
           {path: 'postNewJob', component: PostNewJobComponent},
            {path: 'ReqJobProposal', component: ReqJobProposalComponent},
            {path: 'viewResponse/:id', component: ViewResponseComponent}
        ]
      },
      {path: 'recruiterHeader', component: RecruiterHeaderComponent,
        children: [
          {path: 'recuiterDashboard', component: RecruiterDashboardComponent},
           {path: 'sendProposal', component: AllSendProposalComponent},
           {path: 'recruiterProfile', component: RecruiterProfileComponent},
           {path: 'ViewProposal/:id', component: ViewProposalComponent},
            {path: 'candidateRecuiter', component: CandidateRecuiterComponent},
            {path: 'recuiterManageJobs', component: RecuiterManageJobsComponent},
            {path: 'supportIssue', component: SupportIssueComponent},
            {path: 'recruiterSchedule', component: RecruiterScheduleComponent},
            {path: 'jobStats/:id', component: JobStatsComponent}
        ]
      },
      
      { path: '',   redirectTo: '/main/home', pathMatch: 'full' }
    ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
