import { Injectable } from '@angular/core';
import { Http,Headers,Response,RequestOptions} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HomeService{
  baseUrl="/";
 private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private http:Http) {
  }

 signupClient(data): Observable <any> {
    let url = this.baseUrl+'user/first_signup';
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({ headers: headers});
    console.log("User Form : " + data);
    return this.http.post(url,data,options)
    .map((response:Response) => {
      let resp = response;
      return resp;
    })
    .catch(this._errorHandler);
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error");
  }






}
