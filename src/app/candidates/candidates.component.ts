import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.component.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {
activeTab=1;
userId;
userList;
//noData= false 
candidatesList=[];
shortListed = [];
onHold = [];
interviewed = [];
hired = [];
noData = false ;
noDataS = false;
noDataI = false;
noDataO = false;
noDataH = false ;
constructor(
  	    private appService:AppService,
        private toastr: ToastrService) {  	    
        this.userId = localStorage.getItem('loginSessId'); 
      }

ngOnInit() {
  	this.getUserList(this.userId);
  }

setUsers(){
  	//console.log(status);
  	//this.candidatesList = [];
    if(!this.noData){
  	for(let i = 0 ; i< this.userList.length ; i++){
  		if(this.userList[i].candidates.status === "0"){this.shortListed.push(this.userList[i])};
      if(this.userList[i].candidates.status === "1"){this.interviewed.push(this.userList[i])};
      if(this.userList[i].candidates.status === "2"){this.onHold.push(this.userList[i])};
      if(this.userList[i].candidates.status === "4"){this.hired.push(this.userList[i])};
  	}
  }
    if(this.shortListed.length === 0)this.noDataS = true;
    if(this.interviewed.length === 0)this.noDataI = true;
    if(this.onHold.length === 0)this.noDataO = true;
    if(this.hired.length === 0)this.noDataH = true;

  	//console.log(this.candidatesList);
  }
getUserList(id){
  	this.appService.getCandidateList(JSON.parse(id)).subscribe((data)=>{
  		console.log(data);
      if(data.responseCode === 204){this.noData = true;}
      else{
  		this.userList = data.result;
  			this.setUsers()}
  	},(error)=>{
  		console.log(error);
  	})
  }
downloadResume(event){
  	console.log(event);
  }
setCV(cv){
  console.log(cv);
      window.open(cv, "_blank");
  }
  changeStatus(status,jobId,candiId){
    this.appService.clientShortListCandidate(status,jobId,candiId,JSON.parse(this.userId)).subscribe((data)=>{
      console.log(data);
     // alert("ShortListed Succesfully");
    this.toastr.success(data.responseMessage);
    this.getUserList(this.userId);
    },(error)=>{
      console.log(error);
    })
   }
}
