import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.component.service'


@Component({
  selector: 'app-candidate-recuiter',
  templateUrl: './candidate-recuiter.component.html',
  styleUrls: ['./candidate-recuiter.component.css']
})
export class CandidateRecuiterComponent implements OnInit {

  public userId:any;
  public manageCandidates:any;

  constructor(private appService:AppService) { 

  		this.userId = localStorage.getItem('loginSessId');
 
  		this.appService.clientShortlistResumes(JSON.parse(this.userId))
		   .subscribe(
		        res => {
		          //var result =JSON.parse(res);
		          this.manageCandidates=res.result;
		          console.log(res);	
		          // alert(JSON.stringify(res));
		          return res;
		        },
		        err => {
		          console.log("Error occured");
		          return err;
		        }
		   );


  }

  ngOnInit() {
  }

}
