import { Injectable } from '@angular/core';
import { Http,Headers,Response,RequestOptions} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class AppService{
 private headers = new Headers({'Content-Type': 'application/json'});
baseUrl ='/' ;
 constructor(private http:Http) {
   }
public static getEnvironmentVariable(value) {
    let serverip = '/';
    
    // let serverip = 'http://79719b07.ngrok.io/';
    // let serverip = 'http://192.168.0.19:8080/';
    // let serverip = 'http://7ed90083.ngrok.io/';
    //http://devexchange.Bitbose.io:36115/swagger/
    return serverip;
  }

postJob(data){
   console.log(data,"enteredddsdggs")
   return this.http.post('/post/client_post',data)
   // .subscribe(
   //      res => {
   //        console.log(res);
   //        return res;
   //      },
   //      err => {
   //        console.log("Error occured");
   //        return err;
   //      }
   //    );
}
getRecuiterDashbordJobs()
{

   return this.http.get('/post/show_recuiter_dashbord_jobs ')
}
getRecruiterViewJob(id:any){
return this.http.get('/post/recruiter_view_job/'+id)
}

getSuggestedIndustries(): Observable <any>{
      let url = this.baseUrl+'post/get_industry_list';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._getSuggestedIndustriesErrorHandler);

    }

    _getSuggestedIndustriesErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
    }
makeUploadRequest( body) : Observable<any> {
    const url =this.baseUrl+ "media/upload";
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.post(url, body, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._uploadErrorHandler);

}

_uploadErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}

candidateBulkUploadRequest( jobId,loginId,filePath) : Observable<any> {
    const url = this.baseUrl+"post/candidateBulkUpload/"+jobId+"/"+loginId;
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    var data={

       "filePath":filePath
    }

    const options = new RequestOptions({headers: headers});
    return this.http.put(url, data, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._candidateBulkUploadRequestErrorHandler);

}

_candidateBulkUploadRequestErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}


recruiterAllJobList() : Observable<any> {
    const url =  this.baseUrl+"post/recruiter_all_job_list";

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);

}
getEditJobsDetails(id,jobId){
    const url =  this.baseUrl+"post/client_get_job_detail/"+jobId+'/'+id;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }
_recruiterAllJobListErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}


editJobPostRequest( jobId,data) : Observable<any> {
    const url =  this.baseUrl+"post/edit_post/"+jobId;
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.put(url,data, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._editJobPostRequestErrorHandler);

}
  
_editJobPostRequestErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}



recruiterManageJobs(loginId) : Observable<any> {
    const url =  this.baseUrl+"post/consultant_manage_jobs/"+loginId;

    return this.http.get(url)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterManageJobsErrorHandler);

}

clientShortlistResumes( loginId) : Observable<any> {
    const url =  this.baseUrl+"post/recruiter_manage_candidate/"+loginId;

    return this.http.get(url)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterManageJobsErrorHandler);

}
_recruiterManageJobsErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}

saveBookMark( id,jobid) : Observable<any> {
    const url = this.baseUrl+"post/bookmark_post";
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

     var body={

       "_id":id,
       "job_id":jobid
    }

    const options = new RequestOptions({headers: headers});
    return this.http.post(url, body, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._saveBookMarkErrorHandler);

}

_saveBookMarkErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}


bookMarkJobList( loginId) : Observable<any> {
    const url =  this.baseUrl+"post/recruiter_bookmark_job_list/"+loginId;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._bookMarkJobListErrorHandler);

}

_bookMarkJobListErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}

clientDashboardCount( loginId) : Observable<any> {
    const url =  this.baseUrl+"post/client_dasboard_count/"+loginId;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._clientDashboardCountErrorHandler);

}

_clientDashboardCountErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}



recruiterDashboardCount( loginId) : Observable<any> {
    const url =  this.baseUrl+"post/recruiter_dasboard_count/"+loginId;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterDashboardCountErrorHandler);

}

_recruiterDashboardCountErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}



clientShortListCandidate( status,jobid,candidateId,id) : Observable<any> {
    const url = this.baseUrl+"post/client_shortlist_candidate";
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

     var body={
      "_id":id,
       "status":status,
       "job_id":jobid,
       "candidate_id":candidateId
    }

    const options = new RequestOptions({headers: headers});
    return this.http.post(url, body, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._clientShortListCandidateErrorHandler);

}

_clientShortListCandidateErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}


cvUploadRequest( form,loginId) : Observable<any> {
    const url = this.baseUrl+"post/upload_candidates_resume/"+loginId;
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.put(url, form, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._cvUploadRequestErrorHandler);

}

_cvUploadRequestErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}


recruiterManageCandidate( loginId) : Observable<any> {
    const url =  this.baseUrl+"post/recruiter_manage_candidate/"+loginId;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterManageCandidateErrorHandler);

}

_recruiterManageCandidateErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}

withdrawCandidate( loginId,candidatesId,jobId) : Observable<any> {
    const url =  this.baseUrl+"post/withdraw_candidates";

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

     var data ={
       "job_id":jobId,
       "_id":loginId,
       "candidates_id":candidatesId
       
     }



    const options = new RequestOptions({headers: headers});
    return this.http.post(url, data,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._withdrawCandidateErrorHandler);

}

_withdrawCandidateErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}


clientDashboardFeed(loginId) : Observable<any> {
    const url =  this.baseUrl+"post/client_dashboard_feed/"+loginId;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._clientDashboardFeedErrorHandler);

}

_clientDashboardFeedErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}



clientViewResumeOnDashBoard(jobId,loginId) : Observable<any> {
    const url =  this.baseUrl+"post/client_view_resume/"+loginId+"/"+jobId;

    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');

    const options = new RequestOptions({headers: headers});
    return this.http.get(url, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._clientViewResumeOnDashBoardErrorHandler);

}

_clientViewResumeOnDashBoardErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
}

changePassword(loginId,form): Observable<any> {
    const url = this.baseUrl+"user/change_password/"+loginId;
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.put(url, form, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._cvUploadRequestErrorHandler);

}


getProfileData(loginUserId,job_id,candidate_id) : Observable<any> {
  const url =  this.baseUrl+"post/fetch_data"+"/"+loginUserId+"/"+job_id+"/"+candidate_id;
    console.log(url);
    const headers = new Headers();

    const options = new RequestOptions({headers: headers});
    return this.http.get(url,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);


}
setInterviewOfSingleCandidate(form):Observable<any>{
       const url =this.baseUrl+ "interview/save_time_slot_job";
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.post(url, form, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._uploadErrorHandler);

  }
getUserEvent(userId):Observable<any>{
     const url =this.baseUrl+ "interview/client_slot_aviable_list/"+ userId;
     const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(url,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }
  getScheduledInterview(userId):Observable<any>{
     const url =this.baseUrl+ "interview/client_interview_dashboard/"+ userId;
     const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(url,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }

  getCandidateList(userId):Observable<any>{
     const url =this.baseUrl+ "post/client_candidate_list/"+ userId;
     const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(url,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }

  getRecruiterCompanyList(id):Observable<any>{
    const url =this.baseUrl+ "post/recruiter_company_calender_list/"+ id;
     const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(url,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
    }
getCompanyEvent(id,cId):Observable<any>{
    const url =this.baseUrl+ "interview/recruiter_calender_list/"+ id +"/"+cId;
     const headers = new Headers();
    const options = new RequestOptions({headers: headers});
    return this.http.get(url,options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }


getSkillsList():Observable<any>{
      const url = this.baseUrl+ "post/newGetSkillsuggestion";
      const headers = new Headers();
      const options = new RequestOptions({headers: headers});
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }

  getSingleJobData(id):Observable<any>{
    const url = this.baseUrl+ "post/showjob/"+id;
      const headers = new Headers();
            const options = new RequestOptions({headers: headers});
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }

  getNotificationsFeedRecuiterDashboard(id):Observable<any>{
    const url = this.baseUrl+ "interview/recruiter_interview_dashboard/"+id;
      const headers = new Headers();
            const options = new RequestOptions({headers: headers});
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }

  getCandidateJobStats(id,jobId):Observable<any>{
  const url = this.baseUrl+ "post/view_job_status_recruiter/"+id+"/"+jobId;
      const headers = new Headers();
            const options = new RequestOptions({headers: headers});
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }
  getSingleJobDetails(id,jobId):Observable<any>{
  const url = this.baseUrl+ "post/view_client_job_response/"+id+"/"+jobId;
      const headers = new Headers();
            const options = new RequestOptions({headers: headers});
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._recruiterAllJobListErrorHandler);
  }
  recruiterProfile(id): Observable <any>{
      let url = this.baseUrl+'user/get_profile/'+id;
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._recruiterProfileErrorHandler);

    }
    _recruiterProfileErrorHandler(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
    }
    
    signupClient(data): Observable <any> {
    let url = this.baseUrl+'user/first_signup';
    let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({ headers: headers});
    console.log("User Form : " + data);
    return this.http.post(url,data,options)
    .map((response:Response) => {
      let resp = response;
      return resp;
    })
    .catch(this._errorHandler);
  }

  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || "Server Error");
  }
  updateProfile(loginId,form): Observable<any> {
    const url = this.baseUrl+"user/edit_profile/"+loginId;
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.put(url, form, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._cvUploadRequestErrorHandler);

}
updatePostJob(loginId,form): Observable<any> {
    const url = this.baseUrl+"post/edit_post/"+loginId;
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.put(url, form, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._cvUploadRequestErrorHandler);

}




//----------------------------------------------Admin---------------------------------------------------------------------------

adminLoginForm(form):Observable<any>{
       const url =this.baseUrl+ "admin/admin_login";
    const headers = new Headers();
   // headers.append('Content-Type', 'multipart/form-data');
    const options = new RequestOptions({headers: headers});
    return this.http.post(url, form, options)
    .map((response:Response) => {
        let resp = response.json();
        return resp;
      })
      .catch(this._uploadErrorHandler);

  }

manageJobsAdminpanel(): Observable <any>{
      let url = this.baseUrl+'admin/admin_full_job_list';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
    _manageJobsAdminPanel(error: Response) {
      console.error(error);
      return Observable.throw(error || "Server Error");
    }
manageUserAdminpanel(): Observable <any>{
      let url = this.baseUrl+'admin/admin_manage_user';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }

  getAllJobsListAdmin(): Observable <any>{
      let url = this.baseUrl+'admin/admin_full_job_list';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
getLastestJobsAdmin(): Observable <any>{
      let url = this.baseUrl+'admin/admin_latest_job_list';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
getDashboardCountAdmin(): Observable <any>{
      let url = this.baseUrl+'admin/admin_dasboard_count';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
resDeskAdmin(): Observable <any>{
      let url = this.baseUrl+'admin/resdesk_admin';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.get(url,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
shortListAdminCandidiate(form): Observable <any>{
      let url = this.baseUrl+'admin/admin_cv_shortlist';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.post(url,form,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
    getUsersOnJob(form): Observable <any>{
      let url = this.baseUrl+'admin/admin_view_resume';
      let headers = new Headers({'Content-Type':'application/json'});
      let options = new RequestOptions({ headers: headers});
      //console.log("Profile Form : " + id);
      return this.http.post(url,form,options)
      .map((response:Response) => {
        let resp = response;

        return resp;
      })
      .catch(this._manageJobsAdminPanel);

    }
}
