import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { HomeService } from '../home/home.component.service';
import { CommonFunctionsService } from '../sheared/index';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AppService } from '../app.component.service'
import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap';



@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {
@ViewChild('consultModal') public modalR: ModalDirective;
@ViewChild('clientModal') public modalC: ModalDirective;
	tab = 1;
	formVal = 1;
  signupSubmitted =false;
  signupSubmitted1 = false;

	 signUpFormCompany: FormGroup;
  signUpFormRecruiter:FormGroup;
  constructor(
 private appService:AppService,
    private homeService:HomeService,
     private router: Router,
     private toastr: ToastrService,
    private commonFunctions: CommonFunctionsService) { }

  ngOnInit() {
  	this.initSignUpFormCompany();
    this.initSignUpFormRecruiter();
  }
  onSignUpAsCompany() {
    this.signupSubmitted = true;

    // && this.captcha
    if (this.signUpFormCompany.valid) {

      var control = this.signUpFormCompany.controls;
       var form = {
        'user_name': control.username.value,
        'company_name': control.companyName.value,
        'email_id': control.email.value,
        'user_type': "0",
        'mobile': control.contactNo.value,
        "password":control.password.value
      }
      this.appService.signupClient(form).subscribe(objS=>{
      var resMessage = objS.json();
       console.log(objS);
      this.signupSubmitted = false;
      this.toastr.success("Please check your mail inbox");
      this.signUpFormCompany.reset();
      this.modalC.hide();
      this.router.navigate(['/main/login']);

    },objE=>{
      console.log(objE);
    })
    }
  }
  onSubmitSignUpFormRecruiter(){
    console.log('click doneeeee!!!!!!')
  this.signupSubmitted1 = true;
    // && this.captcha
    if (this.signUpFormRecruiter.valid) {
      var control = this.signUpFormRecruiter.controls;
      var form = {
        'user_name': control.username.value,
        'type': "Individual",
        'email_id': control.email.value,
        'user_type': "1",
        'mobile': control.contactNo.value,
        "password":control.password.value
      }
      this.appService.signupClient(form).subscribe(objS => {
      var resMessage = objS.json();
      console.log(objS);
      this.toastr.success("Please check your mail inbox");
      this.signUpFormRecruiter.reset();
      this.modalR.hide();

      this.signupSubmitted = false;
              this.router.navigate(['/main/login']);

    }, objE=>{
      console.log(objE);
    })

    }
  }
  submittedF(){
    this.signupSubmitted =false;
    this.signupSubmitted1 = false;

  }
initSignUpFormRecruiter(){
  this.signUpFormRecruiter = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.fullNameRegex)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.emailRegex)
      ]),
      contactNo: new FormControl('', [
       Validators.required,
        Validators.pattern(this.commonFunctions.contactRegex),
        Validators.minLength(7),
        Validators.maxLength(15)
        ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
  confirmPassword: new FormControl('', Validators.required)}
  )}

initSignUpFormCompany(){
  this.signUpFormCompany = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.fullNameRegex)
      ]),
      companyName :new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.fullNameRegex)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.emailRegex)
      ]),
      contactNo: new FormControl('', [
       Validators.required,
        Validators.pattern(this.commonFunctions.contactRegex),
        Validators.minLength(7),
        Validators.maxLength(15)
        ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
  confirmPassword: new FormControl('', Validators.required)}
  )}
}
