import { Component, OnInit } from '@angular/core';
import { PostjobService } from './postjob.service'
import {IMyDpOptions} from 'mydatepicker';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm, FormArray, FormBuilder } from '@angular/forms';
import { CompleterService, CompleterData, CompleterItem } from 'ng2-completer';
import { AppService } from '../app.component.service';
import { Options } from 'ng5-slider';
import { ToastrService } from 'ngx-toastr';


import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
@Component({
  selector: 'app-post-new-job',
  templateUrl: './post-new-job.component.html',
  styleUrls: ['./post-new-job.component.css']
})
export class PostNewJobComponent implements OnInit {
  value: number = 5;
  options: Options = {
    floor: 0,
    ceil: 10
  };
  postNewJobForm1 : FormGroup;
  postNewJobForm2 : FormGroup;
  postNewJobForm3 : FormGroup;
  postNewJobForm4 : FormGroup;
  activeTab = '1';
  skills = [];
  industries:any=[]
  functionalArea:any=[]
  qualifications:any=[]
  spans:any=[]
  currencies:any=[]
  periods:any=[];
  doc ;
  step1:boolean=true;
  step2:boolean=false;
  step3:boolean=false;
  step4:boolean=false;
public items = [
    // {display: 'NodeJs', value: 'NodeJs'},
    // {display: 'Angular', value: 'Angular'},
    // {display: 'JavaScript', value: 'JavaScript'},
  ];
  user:any={}
  commission;
  file: File;
    fileError=false;
      typeImage: any;
      fileArray=[];
     fileName;
  base64textString: String = '';
  control1;
  control2;
  control3;
  control4;
  submitted = false;
  submitted2 = false;
  submitted3 = false;
  submitted4 = false;
public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd.mm.yyyy',
    };
public model: any = { date: { year: 2018, month: 10, day: 9 } };
  constructor(
    private postjobService:PostjobService,
    private appService:AppService,
    private completerService: CompleterService,private router: Router,
       private toastr: ToastrService)
     {
     this.getUser();
     this.postjobService.getSuggestedIndustries()
     .subscribe(
        res => {
          this.industries=res.result;
          console.log(res);
          return res;
        },
        err => {
          console.log("Error occured");
          return err;
        }
      );
   }
ngOnInit() { 
    this.initForms();
    this.industries=['first','second','third']
    this.functionalArea=[
                  {value:'Any'},
                  {value:'Accounting / Tax / Company Secretary / Audit'},
                  {value:'Agent'},
                  {value:'Airline / Reservations / Ticketing / Travel'},
                  {value:'Analytics & Business Intelligence'},
                  {value:'Anchoring / TV / Films / PRODUCTION'},
                  {value:'Architects / Interior Design / Naval Arch.'},
                  {value:'Art Director / Graphic / Web Designer'},
                  {value:'Banking / Insurance'},
                  {value:'Content / Editors / Journalists'},
                  {value:'Corporate Planning / Consulting / Strategy'},
                  {value:'Entrepreneur / Businessman / Outside Management Consultant'},
                  {value:'Export / Import'},
                  {value:'Fashion'},
                  {value:'Front Office Staff / Secretarial / Computer Operator'},
                  {value:'Hotels / Restaurant Management'},
                  {value:'HR / Admin / PM / IR / Training'},
                  {value:'ITES / BPO / Operations / Customer Service / Telecalling'},
                  {value:'Legal / Law'},
                  {value:'Medical Professional / Healthcare Practitioner / Technician'},
                  {value:'Mktg / Advtg / MR / Media Planning / PR / Corp. Comm.'},
                  {value:'Packaging Development'},
                  {value:'Production / Service Engineering / Manufacturing / Maintenance'},
                  {value:'Project Management / Site Engineers'},
                  {value:'Purchase / SCM'},
                  {value:'R&D / Engineering Design'},
                  {value:'Sales / Business Development / Client Servicing'},
                  {value:'Security'},
                  {value:'Shipping'},
                  {value:'Software Development - ALL'},
                  {value:'Software Development - Application Programming'},
                  {value:'Software Development - Client Server'},
                  {value:'Software Development - Database Administration'},
                  {value:'Software Development - e-commerce / Internet Technologies'},
                  {value:'Software Development - Embedded Technologies'},
                  {value:'Software Development - ERP / CRM'},
                  {value:'Software Development - Network Administration'},
                  {value:'Software Development - Others'},
                  {value:'Software Development - QA and Testing'},
                  {value:'Software Development - System Programming'},
                  {value:'Software Development - Telecom Software'},
                  {value:'Software Development - Systems / EDP / MIS'},
                  {value:'Teaching / Education / Language Specialist'},
                  {value:'Telecom / IT-Hardware / Tech. Staff / Support'},
                  {value:'Top Management'},
                  {value:'Any Other'}
                  ]
    this.qualifications=['graduate','post-graduate']
    this.currencies=['US Dollar','Indian rupee', 'AED']
    this.periods=['15 days','30 days','1 weak']
    this.spans=['0','2-5','5-8 yrs','8-10 yrs','more']
    this.getDataList();
  }
onModuleSelect(evnt){
    console.log(evnt);
  }
getDataList(){
    this.appService.getSkillsList().subscribe((data)=>{
      this.items = data.result;
      console.log(data); 
      for(let i = 0; i < this.items.length; i++){
      this.items[i].display = this.items[i]['_id'];
      this.items[i].value = this.items[i]['item_id'];
      delete this.items[i]._id;
      delete this.items[i].item_id;
       }
         console.log(this.items);
    })

   }
getUser(): any {
    var user = localStorage.getItem('loginSessId');
    if(user){
      user = JSON.parse(user)
    }
   this.user=user
  }


  nextPage(a){
    this.activeTab = a;
    this.step1=true
    this.step2=false
    this.step3=false
    this.step4=false
  }
  nextPage2(a){
    console.log("step2")
    this.activeTab = a;
     this.step1=false
     this.step2=true
     this.step3=false
     this.step4=false
  }
  nextPage3(a){
    this.activeTab = a;

   this.step1=false
    this.step2=false
    this.step3=true
    this.step4=false
  }
  nextPage4(a){
    
    this.activeTab = a;
    this.step1=false
    this.step2=false
    this.step3=false
    this.step4=true
  }
  onOptionsSelected(event){
  	console.log(event)
  }
  onItemAdded(evt){
    console.log(evt);
    this.skills.push(evt.display)
    console.log(this.skills)
  }
  onItemRemoved(evt){
    console.log(evt);
    var index = this.skills.indexOf(evt.display);
    if (index > -1) {
    this.skills.splice(index, 1);
  }
  console.log(this.skills);
}
  setCommission(evt){
    console.log(evt)
    this.commission = evt.value;

  }
  handleFileSelect(evt) {
    console.log(evt);
    this.fileError = false;
    console.log(evt)
    this.fileArray.push(evt.target.files[0].name);
    this.fileName = evt.target.files[0].name;
    let ext = this.fileName.substr(this.fileName.lastIndexOf('.') + 1);
    if (ext === 'pdf' ) {
      const files = evt.target.files;
      const file = files[0];
      this.typeImage = files[0].type;
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.fileError = false;
      alert('Invalid File Type! Only pdf allowed')
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    if (this.base64textString) {
        this.doc = this.base64textString
      }
    }
      
  nextStep2(tab){
    this.submitted = true ;
    console.log(tab);
      if (this.postNewJobForm1.valid) {
      this.control1 = this.postNewJobForm1.controls
      this.activeTab = tab;
     this.step1=false
     this.step2=true
     this.step3=false
     this.step4=false
     }
  }
  nextStep3(tab){
    this.submitted2 = true ;
    console.log(tab);
      if (this.postNewJobForm2.valid) {
      this.control2 = this.postNewJobForm2.controls
      this.activeTab = tab;
      this.step1=false
      this.step2=false
      this.step3=true
      this.step4=false
     }
  }
  nextStep4(tab){
    this.submitted3 = true ;
    console.log(tab);
      if (this.postNewJobForm3.valid) {
      this.control3 = this.postNewJobForm3.controls
      this.activeTab = tab;
     this.step1=false
     this.step2=false
     this.step3=false
     this.step4=true
     }
  }
  postNewJob(){
    this.submitted4 = true ;
    
      if (this.postNewJobForm4.valid) {
      this.control4 = this.postNewJobForm4.controls
       var userDetail={
      "ctc_min":this.control3.min.value,
      "ctc_max":this.control3.min.value,
     "perks":this.control3.perks.value,
     "user_id":this.user,
     "job_title":this.control1.jobTitle.value,
     "industry":this.control1.industry.value,
      "functional_area":this.control1.functionalArea.value,
     "skills":this.skills,
      "vacancy":this.control1.vacancy.value,
     "Location":this.control1.location.value,
      "qulification":this.control2.qualifications.value,
     "experience":this.control2.experiance.value,
     "description":this.control4.companyDescription.value,
      "ctc":this.control3.ctcType.value,
     "company_name":this.control4.companyName.value,
      "commission":this.commission,
     "deadline":this.control4.deadline.value,
      "notice_period":this.control4.noticePeriod.value,
      "contact_period":this.control4.contactPeriod.value,
      "doc":this.doc
  }
   this.postjobService.postJob(userDetail)
   .subscribe(
        res => {
          console.log(res);
          this.toastr.success("job has been proposed")
          this.submitted4 = false;
          this.initForms();
          this.router.navigate(['/clientHeader/manageJobs']);
          return res;
        },
        err => {
          this.toastr.warning("Error occured");
          return err;
        }
      );
    console.log(userDetail);
    }
  }
  initForms(){
    this.postNewJobForm1 = new FormGroup({
      jobTitle: new FormControl('', [
        Validators.required
      ]),
      industry :new FormControl('', [
        Validators.required
      ]),
      functionalArea: new FormControl('', [
        Validators.required
      ]),
      skills: new FormControl('', [
       Validators.required
      ]),
      vacancy: new FormControl('', [
       Validators.required
      ]),
      location: new FormControl('', [
        Validators.required
      ])
    })
  
    this.postNewJobForm2=new FormGroup({
      qualifications: new FormControl('', [
        Validators.required
      ]),
      experiance :new FormControl('', [
        Validators.required
      ])
    })

    this.postNewJobForm3 =new FormGroup({
      ctcType: new FormControl('', [
        Validators.required
      ]),
      min :new FormControl('', [
        Validators.required
      ]),
      max: new FormControl('', [
        Validators.required
      ]),
      perks: new FormControl('', [
       Validators.required
      ])
    })

    this.postNewJobForm4 =new FormGroup({
      deadline: new FormControl('', [
        Validators.required
       ]),
      companyName :new FormControl('', [
        Validators.required
      ]),
      companyDescription: new FormControl('', [
        Validators.required
      ]),
      noticePeriod: new FormControl('', [
       Validators.required
      ]),
      contactPeriod: new FormControl('', [
        Validators.required
      ])
    })

  }


}
