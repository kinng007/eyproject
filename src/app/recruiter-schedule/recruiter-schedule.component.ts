import { Component,  OnInit,  ChangeDetectionStrategy,  ViewChild,  TemplateRef,  ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { AppService } from '../app.component.service';
import { ToastrService } from 'ngx-toastr';
import {  startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth,addHours} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {  CalendarEvent,  CalendarEventAction, CalendarEventTimesChangedEvent,  CalendarView} from 'angular-calendar';
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-recruiter-schedule',
  templateUrl: './recruiter-schedule.component.html',
  styleUrls: ['./recruiter-schedule.component.css']
})
export class RecruiterScheduleComponent implements OnInit {
jobId;
  candidateID;
  userId;
  editMode = false;
  companyNameList;
  customerDetails;
  jobTitle;
  public currentCompany;
  CandiName="";
  eventArray= [] ;
  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
      date: Date = new Date();
    settings = {
        bigBanner: true,
        timePicker: false,
        format: 'dd-MM-yyyy',
        defaultOpen: true
    }
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        //this.handleEveventArrayent('Deleted', event);
      }
    }
  ];
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [];
  modes = [{"value":"Skype"},{"value":"Face To Face"}]
  userDetailsForm : FormGroup;
  submitted = false;
  activeDayIsOpen: boolean = true;
  constructor(private modal: NgbModal,
    private route: ActivatedRoute,
    private appService:AppService,
    private router: Router,
    private toastr: ToastrService)
    // private toastr: ToastsManager, 
    // private _vcr: ViewContainerRef) {
    // this.toastr.setRootViewContainerRef(_vcr); 
    {
      this.userId = localStorage.getItem('loginSessId');
}

  ngOnInit() {
      this.route.params
      .subscribe(
        (params: Params) => {
          this.jobId = params.jobId;
          this.candidateID = params.id;
          this.editMode = params.id != null;
        //  console.log(this.jobId,this.candidateID)
        //  this.getEventsUser(this.userId);
        });
      // setTimeout(()=>{
      //         this.getEventsUser(this.companyNameList[0].job_id._id,this.userId)

      //       }, 500)
      this.getCompanyList();
        //  this.initForm();
       //   this.getDetails();
  }
dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
   console.log(action, event)
    this.CandiName = event.title ;  
    this.jobTitle = event.start;
  }

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }
  getEventsUser(companyId,id){
    this.currentCompany = companyId;
    this.events = []
    this.appService.getCompanyEvent(JSON.parse(id),companyId).subscribe((data)=>{
      this.eventArray = data.result;
       setTimeout(() => {
              this.postEventsOnCalender();
          }, 500);
      this.toastr.success("Success", 'Check all scheduled Interview.');
       console.log(data) ;
    },(error)=>{
      console.log(error);
    })
  }
postEventsOnCalender(){
  console.log("entry filing")
   for(let i = 0 ; i< this.eventArray.length ; i++){
    console.log(i);
   this.events.push({
      title: " Candidate "+this.eventArray[i].candidate_name+"'s Interview scheduled For "+this.eventArray[i].job_id.job_title + " Job ",
      start: this.eventArray[i].interview_date,
      end: addHours(this.eventArray[i].interview_date, 1),  
      color: colors.red,
      draggable: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      }
    });
    this.refresh.next();
    console.log(this.events);
   }       
   this.refresh.next();

  }
getCompanyList(){

	this.appService.getRecruiterCompanyList(JSON.parse(this.userId)).subscribe((data)=>{
		this.companyNameList = data.result;
				console.log(data);

		console.log(this.companyNameList[0].job_id._id, this.userId);
		this.getEventsUser(this.companyNameList[0].job_id._id, this.userId );
		console.log(data);
	},(error) => {
		console.log(error);
	})	
	}

}

