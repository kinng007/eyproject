import { Component,  OnInit,  ChangeDetectionStrategy,  ViewChild,  TemplateRef} from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { AppService } from '../app.component.service';

import {  startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth,addHours} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {  CalendarEvent,  CalendarEventAction, CalendarEventTimesChangedEvent,  CalendarView} from 'angular-calendar';
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-schedule',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  jobId;
  candidateID;
  userId;
  jobTitle;
  CandiName="";
  editMode = false;
  customerDetails;
  eventArray ;
 @ViewChild('modalContent')
  modalContent: TemplateRef<any>;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
      date: Date = new Date();
    settings = {
        bigBanner: true,
        timePicker: false,
        format: 'dd-MM-yyyy',
        defaultOpen: true
    }
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [];
  modes = [{"value":"Skype"},{"value":"Face To Face"}]
  userDetailsForm : FormGroup;
  submitted = false;
  activeDayIsOpen: boolean = true;
  constructor(private modal: NgbModal,
    private route: ActivatedRoute,
    private appService:AppService,
    private router: Router) { 
      this.userId = localStorage.getItem('loginSessId');
  }
ngOnInit() {
      this.initForm();
      this.route.params
      .subscribe(
        (params: Params) => {
          this.jobId = params.jobId;
          this.candidateID = params.id;
          this.editMode = params.id != null;
          //console.log(this.jobId,this.candidateID)
            });
      if(this.editMode){
          this.getEventsUser(this.userId);
          //this.initForm();
          this.getDetails();
        }
      if(!this.editMode){
        console.log('normal');
        this.getEventsUser(this.userId);
      }
     }
dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

handleEvent(action: string, event: CalendarEvent): void {
    //this.modalData = { event, action };
    console.log(action, event)
    this.CandiName = event.title ;  
    this.jobTitle = event.start;
  //  this.modal.open(this.modalContent, { size: 'lg' });
  }
addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }
getDetails(){
  console.log(JSON.parse(this.userId),this.jobId,this.candidateID);
  this.appService.getProfileData(JSON.parse(this.userId),this.jobId,this.candidateID).subscribe((data)=>{
    this.customerDetails = data.result[0];
    console.log(this.customerDetails);
    this.setUserForm();
  },(error)=>{
    console.log(error);
  })
}
initForm(){
 
this.userDetailsForm = new FormGroup({
      title: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      mode: new FormControl('', Validators.required),
      location: new FormControl('', Validators.required),
      date: new FormControl('',Validators.required),
      //time:new FormControl('',Validators.required),
       })
  }
onSubmit(){
    this.submitted = true;
  console.log(this.userDetailsForm)
  var control = this.userDetailsForm.controls;
  var form = {
  "job_id":this.jobId,
  "client_id":this.customerDetails.client_id,
  "candidate_id" : this.candidateID,
  "location":control.location.value,
  "interview_type":control.mode.value,
  "interview_date":control.date.value,
  "interview_time":control.date.value,
  "candidate_name":control.name.value
  }
  console.log(form);

  this.appService.setInterviewOfSingleCandidate(form).subscribe((data)=>{
    alert("Schedule Succesfully");
    console.log(data);
  },(error)=>{
    console.log(error)
  })
  }
setUserForm(){
    if(this.editMode){
      console.log("in editmode");
      this.userDetailsForm.patchValue({
        title:this.customerDetails.job_id[0].job_title,
        name:this.customerDetails.candidates.name,
        location:"",
        mode : "",
        data :"",
      })
    }else{
      console.log("editMode is off");
    }
  }
getEventsUser(id){
    this.appService.getUserEvent(JSON.parse(id)).subscribe((data)=>{
      this.eventArray = data.result;
       setTimeout(() => {
              this.postEventsOnCalender();
          }, 500);
    
       console.log(data) ;
    },(error)=>{
      console.log(error);
    })
  }
postEventsOnCalender(){
  console.log("entry filing")
   for(let i = 0 ; i< this.eventArray.length ; i++){
    console.log(i);
   this.events.push({
      title: " Candidate "+this.eventArray[i].candidate_name+"'s Interview is scheduled For "+this.eventArray[i].job_id.job_title +" Job ",
      start: this.eventArray[i].interview_date,
      end: addHours(this.eventArray[i].interview_date, 1),  
      color: colors.red,
      draggable: false,
      resizable: {
        beforeStart: false,
        afterEnd: false
      }
    });
    this.refresh.next();
    console.log(this.events);
   }       
   this.refresh.next();
  }

}
