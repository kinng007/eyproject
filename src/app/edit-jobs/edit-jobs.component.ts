import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { AppService } from '../app.component.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-jobs',
  templateUrl: './edit-jobs.component.html',
  styleUrls: ['./edit-jobs.component.css']
})
export class EditJobsComponent implements OnInit {
userId;
jobId;
editPostJobForm:FormGroup;
skills = [];
values;
industries:any=[]
functionalArea:any=[]
qualifications:any=[]
spans:any=[]
currencies:any=[]
periods:any=[];
submitted = false;

constructor(
  	private modal: NgbModal,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private appService:AppService,
    private router: Router
    ) {
  		this.userId = localStorage.getItem('loginSessId');
      this.appService.getSuggestedIndustries()
     .subscribe(
        res => {
          this.industries=res.result;
          console.log(res);
          return res;
        },
        err => {
          console.log("Error occured");
          return err;
        }
      );
     }

  ngOnInit() {
  	this.route.params
      .subscribe(
        (params: Params) => {
          this.jobId = params.jobId;
          console.log(this.jobId);
      });
      this.initForm();
      this.getdata();
    this.functionalArea=[
                  {value:'MARKETING/PROMOTION'},
                  {value:'PRODUCTION'},
                  {value:'SALES'},
                  {value:'CUSTOMER SERVICE SUPPORT'},
                  {value:'ACCOUNTING AND FINANCE'}]
    this.qualifications=['graduate','post-graduate']
    this.currencies=['US Dollar','Indian rupee']
    this.periods=['15 days','30 days','1 weak']
    this.spans=['0','2-5','5-8 yrs','8-10 yrs','more']
  }
initForm(){
     this.editPostJobForm = new FormGroup({
      jobTitle: new FormControl('', [
        Validators.required
      ]),
      industry :new FormControl('', [
        Validators.required
      ]),
      functionalArea: new FormControl('', [
        Validators.required
      ]),
      skills: new FormControl('', [
       Validators.required
      ]),
      vacancy: new FormControl('', [
       Validators.required
      ]),
      location: new FormControl('', [
        Validators.required
      ]),
      qualifications: new FormControl('', [
        Validators.required
      ]),
      experiance :new FormControl('', [
        Validators.required
      ]),
      ctcType: new FormControl('', [
        Validators.required
      ]),
      min :new FormControl('', [
        Validators.required
      ]),
      max: new FormControl('', [
        Validators.required
      ]),
      perks: new FormControl('', [
       Validators.required
      ]),
      deadline: new FormControl('', [
        Validators.required
       ]),
      companyName :new FormControl('', [
        Validators.required
      ]),
      companyDescription: new FormControl('', [
        Validators.required
      ]),
      noticePeriod: new FormControl('', [
       Validators.required
      ]),
      contactPeriod: new FormControl('', [
        Validators.required
      ])
    })
  }
getdata(){
  this.appService.getEditJobsDetails(this.jobId,JSON.parse(this.userId)).subscribe((data)=>{
    this.values = data.result;
    console.log(data);
    console.log(this.values);
    this.setFormValues();
    console.log(data);
  },(err)=>{
    console.log(err);
  })
}
setFormValues(){
  this.editPostJobForm.setValue({
    jobTitle:this.values.job_title,
    industry:this.values.industry,
    vacancy:this.values.vacancy,
    skills :this.values.skills,
    functionalArea:this.values.functional_area,
    location:this.values.Location,
    qualifications:this.values.qulification,
    experiance:this.values.experience,
    ctcType:"US Dollar",
    min:this.values.ctc_min,
    max:this.values.ctc_max,
    perks:this.values.perks,
    deadline:this.values.deadline,
    companyName:this.values.company_name,
    companyDescription:this.values.description,
    noticePeriod:this.values.notice_period,
    contactPeriod:this.values.contact_period
  })
}
updateJob(){
  this.submitted = true ;
    
      if (this.editPostJobForm.valid) {
      let control = this.editPostJobForm.controls
       var form={
     "ctc_min":control.min.value,
     "ctc_max":control.max.value,
     "perks":control.perks.value,
     "user_id":this.userId,
     "job_title":control.jobTitle.value,
     "industry":control.industry.value,
     "functional_area":control.functionalArea.value,
     "skills":control.skills.value,
     "vacancy":control.vacancy.value,
     "Location":control.location.value,
     "qulification":control.qualifications.value,
     "experience":control.experiance.value,
     "description":control.companyDescription.value,
     "ctc":control.ctcType.value,
     "company_name":control.companyName.value,
     "deadline":control.deadline.value,
     "notice_period":control.noticePeriod.value,
     "contact_period":control.contactPeriod.value,
        }
       this.appService.updatePostJob(this.jobId,form).subscribe((Data)=>{
        console.log(Data),
        this.submitted = false;
        this.toastr.success("Changed Job Successfully");
        this.router.navigate(['/clientHeader/manageJobs']);
       },(error)=>{
        console.log(error)
       }) 
      }
    }
}