import { Component, OnInit } from '@angular/core';
import { PostjobService } from '../post-new-job/postjob.service';
import { Http } from "@angular/http";
import { AppService } from '../app.component.service'
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-view-proposal',
  templateUrl: './view-proposal.component.html',
  styleUrls: ['./view-proposal.component.css']
})
export class ViewProposalComponent implements OnInit {
        candidates=[];
      dashboardJobs:any=[];
      file: File;
      fileError=false;
      typeImage: any;
      fileArray=[];
      fileName;
       p = 1;
       caId;
       id;
       ID;
       Jd;
       showJd=false;
       List = [];
       base64textString: String = '';
     
    public userId:any;
 constructor(
  private appService:AppService,
  private appService2 :PostjobService,
  private router: Router,
  private route: ActivatedRoute ) { 

  this.userId = localStorage.getItem('loginSessId');
}
 
  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params.id;
        }
      );
    this.appService.getSingleJobData(this.id).subscribe((data)=>{
      console.log(data);
      this.dashboardJobs = data.result
      if(this.dashboardJobs.doc != '')this.showJd = true;
      this.Jd = this.dashboardJobs.doc
    },(error)=>{
      console.log(error);
    })

  }

  handleFileSelect(evt,id) {
    console.log(evt);
    console.log(id);
    this.caId = id;
    this.fileError = false;
    console.log(evt)
    this.fileArray.push(evt.target.files[0].name);
    this.fileName = evt.target.files[0].name;
    let ext = this.fileName.substr(this.fileName.lastIndexOf('.') + 1);
    if (ext === 'pdf' ) {
      const files = evt.target.files;
      const file = files[0];
      this.typeImage = files[0].type;
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.fileError = false;
      alert('Invalid File Type! Only pdf allowed')
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    if (this.base64textString) {
      var form = {
        'cv': this.base64textString,
        'candidates_id': this.caId
      };
      console.log('imgform', form);
      
      this.appService.cvUploadRequest(form,this.ID).subscribe((data)=>{
        console.log(data);
      },(error)=>{
        console.log(error);
      })
        // this.correct = true;
        // this.appService.uploadCoinImage(form).subscribe((data) => {
        //     // console.log(data);
        //     console.log(data);
        //   },
        //   (error) => {
        //     console.log(error);
        //   });
    }
  }  
  onChange(event: EventTarget) {
        let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
        let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
        let files: FileList = target.files;
        this.file = files[0];
        console.log(this.file);
    }

 onSubmit(jobId){
      const formdata: FormData = new FormData();
      console.log(this.file)
      formdata.append('files', this.file);

      this.appService.makeUploadRequest(formdata).subscribe(response => {
      console.log(response)

          this.appService.candidateBulkUploadRequest(jobId,JSON.parse(this.userId),response.file).subscribe(response => {
           this.candidates = response[0].candidates;
           console.log(this.candidates);
           console.log(response);
           console.log(this.candidates);
           this.ID = response[0]._id;
          },(error) =>{
            console.log(error)
          });
        },(err) => {
          console.log(err);
        });
    }


  generateXLSX() {
    let newList = [{
      NAME:'',
      AGE:'',
      SEX:'',
      EMAIL:'',
      EXPERIENCE:'',
      PHONE_NUMBER:'',
      QUALIFICATION:'',
      CURRENT_ORGANISATION:'',
      CTC:''
     }]
    
    // newList = data.map(list => ({
    //   Date: 'list.createDataTime',
    //   BlockChain_Balance: list.blockchainWalletBalance,
    //   Address: list.walletAddress,
    //   Balance: list.walletBalance,
    //   Code: list.walletCode,
    //   Name: list.walletName,
    //   Status: list.status
    // }));
    console.log(newList);
    var array = typeof newList !== 'object' ? JSON.parse(newList) : newList;
    var str = '';
    var row = '';

    for (var index in newList[0]){
      row += index + '\t';
      console.log(row);
    }
    row = row.slice(0, -1);
    str += row + '\r\n';

    for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
        if (line !== '') line += '\t';

        line += array[i][index];
      }
      str += line + '\r\n';
    }
    console.log(str);
    return str;
  }

  download() {
    var csvData = this.generateXLSX();
    var a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    var blob = new Blob([csvData], { type: 'text/xlsx' });
    var url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = 'List.xlsx';
    a.click();
    return 'success';
  }
  getDataAndDownlaodEXCEL() {
    this.List = [];
      this.download()
  }
  viewJD(){
     window.open(this.Jd, "_blank");
  }
}
