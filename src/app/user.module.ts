import { BrowserModule } from '@angular/platform-browser';
import { RouterModule} from '@angular/router';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { Ng2CompleterModule } from "ng2-completer";

import { Ng5SliderModule } from 'ng5-slider';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import {
  SocialLoginModule, 
  AuthServiceConfig,
  GoogleLoginProvider, 
  FacebookLoginProvider, 
  LinkedinLoginProvider,

} from 'ng4-social-login';
import { Component, OnInit, Output, EventEmitter,NgModule,Injectable} from '@angular/core';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

import { BarRatingModule } from "ngx-bar-rating";

import { TagInputModule } from 'ngx-chips';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RecruiterDashboardComponent } from './recruiter-dashboard/recruiter-dashboard.component';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { ClientHeaderComponent } from './client-header/client-header.component';
import { RecruiterHeaderComponent } from './recruiter-header/recruiter-header.component';
import { ManageJobsComponent } from './manage-jobs/manage-jobs.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { SupportIssueComponent } from './support-issue/support-issue.component';
import { ScheduleComponent } from './schedule/schedule.component';
import swal from 'sweetalert';
import { PostNewJobComponent } from './post-new-job/post-new-job.component';
import { ReqJobProposalComponent } from './req-job-proposal/req-job-proposal.component';
import { ViewResponseComponent } from './view-response/view-response.component';
import { AllSendProposalComponent } from './all-send-proposal/all-send-proposal.component';
import { RecruiterProfileComponent } from './recruiter-profile/recruiter-profile.component';
import { ViewProposalComponent } from './view-proposal/view-proposal.component';
import { CandidateRecuiterComponent } from './candidate-recuiter/candidate-recuiter.component';
import { RecuiterManageJobsComponent } from './recuiter-manage-jobs/recuiter-manage-jobs.component';
import { RecruiterScheduleComponent } from './recruiter-schedule/recruiter-schedule.component';
import { ViewResumeComponent } from './view-resume/view-resume.component';
import { JobStatsComponent } from './job-stats/job-stats.component';
import { AboutComponent } from './about/about.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { FaqComponent } from './faq/faq.component';
import { RecruiterSignUpComponent } from './recruiter-sign-up/recruiter-sign-up.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeService } from './home/home.component.service';
import { LoginService } from './login/login.service';
import { HttpModule,Http } from '@angular/http'; 
import {NgxPaginationModule} from 'ngx-pagination';
import { CommonFunctionsService} from './sheared';
import { UserRoutingModule } from './user-routing.module';

import { HttpClient } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { AppService } from './app.component.service'
//import { Ng5SliderModule } from 'ng5-slider';
import { MyDatePickerModule } from 'mydatepicker';
import { TermsComponent } from './terms/terms.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

import 'flatpickr/dist/flatpickr.css';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
 import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ContactUsComponent } from './contact-us/contact-us.component';

import { ToastrModule } from 'ngx-toastr';


const CONFIG = new AuthServiceConfig([
  {
    id: LinkedinLoginProvider.PROVIDER_ID,
    provider: new LinkedinLoginProvider('81r3ougl97r3pk')
  }
],null);
 @NgModule({
  
  declarations: [
    HomeComponent,
    RecruiterDashboardComponent,
    ClientDashboardComponent,
    ClientHeaderComponent,
    RecruiterHeaderComponent,
    ManageJobsComponent,
    CandidatesComponent,
    SupportIssueComponent,
    ScheduleComponent,
    PostNewJobComponent,
    ReqJobProposalComponent,
    ViewResponseComponent,
    AllSendProposalComponent,
    RecruiterProfileComponent,
    ViewProposalComponent,
    CandidateRecuiterComponent,
    RecuiterManageJobsComponent,
    RecruiterScheduleComponent,
    ViewResumeComponent,
    JobStatsComponent,
    AboutComponent,
    ContactUsComponent,
    MainHeaderComponent,
    FaqComponent,
    RecruiterSignUpComponent,
    LoginComponent,
    TermsComponent,
    PrivacyPolicyComponent,
    ContactUsComponent
    ],
      imports: [,
    UserRoutingModule,
    TagInputModule,
    
    SocialLoginModule,
    AngularDateTimePickerModule,
    TagInputModule,
    NgxPaginationModule,
    CarouselModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    OwlDateTimeModule, 
    PdfViewerModule,
    Ng2CompleterModule,
    Ng5SliderModule,
    OwlNativeDateTimeModule,
    BarRatingModule,
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FlatpickrModule.forRoot(),
    NgbModalModule.forRoot(),
        CalendarModule.forRoot({
          provide: DateAdapter,
          useFactory: adapterFactory
        }),
    MyDatePickerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
	],
 providers: [
     
    HomeService,
    LoginService,
    CommonFunctionsService,
    AppService
	
  ],
})
export class UserModule { }
