import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.component.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css']
})
export class ManageUserComponent implements OnInit {
  users;
  data
  constructor(
  	  private appService:AppService,
  	  private toastr: ToastrService ) { 
  }

  ngOnInit() {
  	this.getUsers();
  }
  getUsers(){
  	this.appService.manageUserAdminpanel().subscribe((Data)=>{
  		this.data = JSON.parse(Data._body);
        this.users = this.data.result
      console.log(this.users)
  	},(err) =>{
  		console.log(err)
  	})
  }
}
