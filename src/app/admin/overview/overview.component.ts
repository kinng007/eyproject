import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.component.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  totolEmployer  = 0;
  totalConsultant = 0;
  count;
  single = [
        {
          "name": "jan",
          "value": 89400
        },
        {
          "name": "feb",
          "value": 50000
        },
        {
          "name": "march",
          "value": 72000
        },
        {
          "name": "april",
          "value": 50000
        },
        {
          "name": "may",
          "value": 50000
        }
      ];
      view: any[] = [400 , 300];

        // options
        showXAxis = true;
        showYAxis = true;
        gradient = false;
        showLegend = true;
        showXAxisLabel = true;
        xAxisLabel = 'Months';
        showYAxisLabel = true;
        yAxisLabel = 'Clients';

         colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  constructor(
  	  private appService:AppService,
  	  private toastr: ToastrService ) {
  	   }

  ngOnInit() {
  	this.getJobsList()
    this.getCount()
  }
  getCount(){
    this.appService.getDashboardCountAdmin().subscribe((data)=>{
  //      console.log(data);
   //     console.log(JSON.parse(data._body));
        this.count  = JSON.parse(data._body);
        this.totalConsultant  = this.count.result.TotalClient;
        this.totolEmployer = this.count.result.TotalRecruiter;
    },(err)=>{
      console.log(err);
    })
  }
  getJobsList(){
  	this.appService.getAllJobsListAdmin().subscribe((data)=>{
	//console.log(data);
  console.log(JSON.parse(data._body));
	},(err)=>{
	console.log(err);
	 })
  }
    onSelect(event) {
    console.log(event);
  }

}
