import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AppService } from '../../app.component.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-resume-admin',
  templateUrl: './view-resume-admin.component.html',
  styleUrls: ['./view-resume-admin.component.css']
})
export class ViewResumeAdminComponent implements OnInit {

 id;
	currentCandidate = 0; 
	ResumeList = [];
  Resume ;
	candidateList = 0;
  jobId;
  candidateId;
  candidateName;
  ID
  CV;
  ShowButton=false;
  constructor(
  	private appService:AppService,
  	private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService) {  
 }

  ngOnInit() {
  	this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params.id;
          console.log(this.id)
          var form = {
          	"job_id":params.id
          }
          this.getFeeds(form);
        });

	  }
getFeeds(form){
  console.log(form);
     this.appService.getUsersOnJob(form).subscribe(async response => {
         console.log("response -----",response)

         this.Resume =JSON.parse(response['_body']);
         //console.log(this.ResumeList);
         this.ResumeList = this.Resume.result
         this.candidateList = this.ResumeList.length;
         await this.getCandidate();
         console.log("meaning full data",this.ResumeList);
    },(error)=>{
      	console.log(error);
          });

       
    };



getUserDetails(){
  console.log(this.candidateList);
  if(this.currentCandidate === this.candidateList-1){
    this.getFeeds(this.id);
     this.currentCandidate = 0;
     this.getCandidate();
  }else{
    this.currentCandidate++;
    this.getCandidate()
  }
  
    // if(this.candidateList = this.currentCandidate+1){
    //    this.currentCandidate = 0;
    //    this.getFeeds(this.id);
    // //this.getCandidate();
    // }
    //   this.currentCandidate++; 
    //   this.getCandidate();
    }



 getCandidate(){
 	console.log(this.currentCandidate);
  this.candidateName = this.ResumeList[this.currentCandidate].candidates.name;
  this.CV = this.ResumeList[this.currentCandidate].candidates.cv;
  this.ID = this.ResumeList[this.currentCandidate]._id;
  console.log(this.candidateName,this.CV);
  this.jobId=this.ResumeList[this.currentCandidate].job_id;
  this.candidateId=this.ResumeList[this.currentCandidate].candidates._id;
  console.log(this.candidateId,this.jobId);
  if(this.ResumeList[this.currentCandidate].candidates.status === '0'){ this.ShowButton = true }
    else{this.ShowButton = false};
  }
 


 onShortList(id,candidateID){
  if(status==="2"){this.ShowButton = false;}
  if(status==="0"){this.ShowButton = true;}
  var form  = 
  	{
  	"_id":this.ID,
  	"candidate_id":this.candidateId
  	}
  
  console.log('enterreeed in side short list');
  this.appService.shortListAdminCandidiate(form).subscribe((data)=>{
      console.log(data);
     // alert("ShortListed Succesfully");
    this.toastr.success(data.responseMessage);

    },(error)=>{
      console.log(error);
    })
   }
}
