import { Component, OnInit } from '@angular/core';
import { RecruiterProfileService } from './recruiter-profile.service';
import { AppService } from '../app.component.service'
import { CommonFunctionsService } from '../sheared/index';
import { FormGroup, FormControl, Validators, NgForm, FormArray, FormBuilder } from '@angular/forms';
import swal from 'sweetalert';
import { ToastrService } from 'ngx-toastr';
import { Title, DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-recruiter-profile',
  templateUrl: './recruiter-profile.component.html',
  styleUrls: ['./recruiter-profile.component.css']
})
export class RecruiterProfileComponent implements OnInit {
user;
Experience=[];
recruiterType=[];
imageUrl;
naukriOrMonstor = [];
ctc = [];
id;
dashboardCount;
wantedCtc = [];
submitted = false ;
TotalPlaced;
Totalshortlist;
commission = [];
industry =[];
TotalCandidate;
eyQualified;
username;
ctcWorked;
commissionUser;
userExperience;
recruiterworkingtype;
otherAccountProfile;
ctcExpected;
expertKeys;
expertIndustry;
userEmail;
userMobile;
profilePicture;
keys=[];
img;
file: File;
fileError=false;
typeImage: any;
fileArray=[];
fileName;
base64textString: String = '';
editForm:FormGroup
constructor(
   private recruiterProfileService :RecruiterProfileService,
   private appService:AppService,
   private commonFunctions: CommonFunctionsService,
   private toastr: ToastrService,
   private _sanitizer: DomSanitizer) { 
    this.id = localStorage.getItem('loginSessId');
   }
ngOnInit() {
  this.initForm();
  this.getuser(this.id);
  this.getRecruiterDasboard(this.id);
  this.Experience = ['Fresher','1-3 years' , '3+ years'];
  this.recruiterType = ['Full Time','Part Time'];
  this.naukriOrMonstor =[{display:'YES', value:true},{display:'NO', value:false}];
  this.ctc = ['3-10','10-20','20-30','30-40','40-50','50+']; 
  this.wantedCtc = ['upto 4','4-10','10-25','25+'];
  this.commission = ['25','30','40','50'];
  }
 handleFileSelect(evt) {
    console.log(evt);
    this.fileError = false;
    console.log(evt)
    this.fileArray.push(evt.target.files[0].name);
    this.fileName = evt.target.files[0].name;
    let ext = this.fileName.substr(this.fileName.lastIndexOf('.') + 1);
    if( ext === 'jpg' || ext === 'jpeg' || ext === 'png'  ) {
      const files = evt.target.files;
      const file = files[0];
      this.typeImage = files[0].type;
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.fileError = false;
      this.toastr.warning('Invalid File Type! Only .jpeg allowed')
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    if (this.base64textString) {
        this.img = this.base64textString;
        this.imageUrl = this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+ this.base64textString);
        this.profilePicture =  this._sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+ this.base64textString);
      }
    }

getuser(id){
     this.recruiterProfileService.recruiterProfile(JSON.parse(id)).subscribe(objS=>{
           var resp = objS.json();
           this.user=resp.result;
           this.username = this.user.user_name;
            this.userMobile = this.user.mobile;
            this.userEmail = this.user.email_id;
            this.ctcWorked = this.user.working_package;
            this.userExperience = this.user.experience;
            this.recruiterworkingtype = this.user.recruiter_working_type;
            this.ctcExpected = this.user.ctc_worked;
            if(this.user.naukri_portal_login)this.otherAccountProfile = "YES";
            if(!this.user.naukri_portal_login)this.otherAccountProfile = "NO";
            this.expertKeys = this.user.industry_expert.toString();
            this.expertIndustry = this.user.functional_expert.toString();
            this.commissionUser = this.user.commision;
            this.profilePicture = this.user.profile_pic;
           console.log(this.user);
        },objE=>{
          console.log(objE)  
        })

   }
initForm(){
    this.editForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.fNameRegex)
      ]),
      contact: new FormControl('', [
        Validators.required,
        Validators.pattern(this.commonFunctions.contactRegex),
        Validators.minLength(7),
        Validators.maxLength(15)
      ]),
       email: new FormControl('', [
        Validators.required
      ]),
      location: new FormControl('', [
        Validators.required
      ]),
      experiance: new FormControl('', [
        Validators.required
      ]),
      recruiterType: new FormControl('', [
       Validators.required
      ]),
      otherAccount: new FormControl('', [
       Validators.required
      ]),
      currentCtc: new FormControl('', [
       Validators.required
      ]),
      expectCtc: new FormControl('', [
       Validators.required
      ]),
      commission: new FormControl('', [
       Validators.required
      ]),
      keys: new FormControl('', [
       Validators.required
      ]),
      industry: new FormControl('', [
       Validators.required
      ]),
      pan: new FormControl('', [
       Validators.required
      ])
    })
   }
setForm(){
  this.editForm.setValue({
    name:this.username,
    contact: this.userMobile,
    email:this.userEmail,
    location:this.user.city,
    experiance:this.userExperience,
    recruiterType:this.recruiterworkingtype,
    otherAccount:this.otherAccountProfile,
    currentCtc: this.ctcWorked,
    expectCtc:this.ctcExpected,
    commission: this.commission,
    keys:this.user.industry_expert,
    industry: this.user.functional_expert,
    pan:this.user.pan_number})
}
updateProfile(){
        this.submitted = true ;
    
      if (this.editForm.valid) {
      let control = this.editForm.controls
      console.log(control)
      var form = {
        'user_name': control.name.value,
        'mobile':JSON.parse(control.contact.value),
        'profile_pic':this.img,
        'experience':control.experiance.value,
        'naukri_portal_login':control.otherAccount.value,
        'city' :control.location  .value,
        'recruiter_working_type':control.recruiterType.value,
        'working_package':control.currentCtc.value,
        'functional_expert' :this.keys,
        'industry_expert' :this.industry,
        'ctc_worked':control.expectCtc.value,
        'commision' : control.commission.value,
        'pan_number' :control.pan.value,
      }
      console.log(form);

       this.appService.updateProfile(JSON.parse(this.id),form).subscribe((data)=>{
        console.log(data)
       },(err)=>{
        console.log(err);
       })
    }
  }
  getRecruiterDasboard(id){
    this.appService.recruiterDashboardCount(JSON.parse(id)).subscribe(res => {
          this.dashboardCount=res.result;
          this.TotalCandidate = this.dashboardCount.TotalcanidateUpload;
          this.eyQualified = this.dashboardCount.Eyqualified;
          this.Totalshortlist = this.dashboardCount.Totalshortlist[0].count;
          this.TotalPlaced = this.dashboardCount.TotalPlaced[0].count;
          console.log(this.dashboardCount);
          return res;
        },err => {
          console.log("Error occured");
          return err;
        }
    );
  }
  onKeysRemoved(evt){
    console.log(evt);
     var index = this.keys.indexOf(evt.display);
    if (index > -1) {
    this.keys.splice(index, 1);}
  }
onIndustryRemoved(evt){
    console.log(evt);
     var index = this.industry.indexOf(evt.display);
    if (index > -1) {
    this.industry.splice(index, 1);}

  }
  onKeysAdded(evt){
    console.log(evt);
     this.keys.push(evt.display)
  }
  onIndustryAdded(evt){
    console.log(evt);
    this.industry.push(evt.display)
  }
}
