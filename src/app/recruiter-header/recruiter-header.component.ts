import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms'
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AppService } from '../app.component.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-recruiter-header',
  templateUrl: './recruiter-header.component.html',
  styleUrls: ['./recruiter-header.component.css']
})
export class RecruiterHeaderComponent implements OnInit {
	activeHead = 1;
  changePasswordForm: FormGroup;
changeSubmitted= false;
  user;
public constructor(
  private router: Router,
  private appService : AppService,
  private toastr: ToastrService) {
  this.user = localStorage.getItem('loginSessId'); }
  
  ngOnInit() {
    this.initChangePassword();
  }
  
  
    // console.log(user)
    // if(user){
    //   user = JSON.parse(user)
    //   return user
    // }
   


onChangePassword() {
    this.changeSubmitted = true;
    var control = this.changePasswordForm.controls;
    if (this.changePasswordForm.valid && control.password.value == control.confirmPassword.value) {
      var form = {
        "oldPassword": control.oldPassword.value,
        "newPassword": control.password.value,
      }
      console.log(form);
      this.appService.changePassword(JSON.parse(this.user),form).subscribe((data)=>{
      this.toastr.success("Change Password Successfully");
        console.log(data);
      },(err)=>{
      this.toastr.success("Invalid Password");
        console.log(err);
      })
    }
  }
  initChangePassword(){
  this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl("", Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('',Validators.required),
       });
}
}
