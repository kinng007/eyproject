import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageJobsService } from './manage-jobs.service'
import { AppService } from '../app.component.service'
import { ToastrService } from 'ngx-toastr';
import { ModalDirective } from 'ngx-bootstrap';
import { NgbModal, ModalDismissReasons, NgbActiveModal, NgbModalRef, NgbModalOptions, NgbTabset } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-manage-jobs',
  templateUrl: './manage-jobs.component.html',
  styleUrls: ['./manage-jobs.component.css']
})
export class ManageJobsComponent implements OnInit {
@ViewChild('verify') public modal: ModalDirective;

modelRef: NgbModalRef;
  public userId:any;
  jobs ;
  jobIdDelete;
  noData = false;
  public updatedJob:any;
  lengthCA ;
  constructor(
  	public manageJobsService : ManageJobsService,
  	private modalService: NgbModal,
  	private appService:AppService,
       private toastr: ToastrService
  	) {
  	    
  	this.userId = localStorage.getItem('loginSessId');



   }

  ngOnInit() {
  	this.getJobs()
  }
   
setId(id){
  this.jobIdDelete = id;
  console.log(this.jobIdDelete)
}

  getJobs(){
  	 this.manageJobsService.manageJob(JSON.parse(this.userId))
	     .subscribe(
	        res => {
	         if(res.responseCode === 204){
            this.noData = true;
           }else{
            this.jobs=res.result;
            this.lengthCA = this.jobs.lenght
            console.log("res-----",this.jobs);
            console.log(this.jobs.job_id)
            return res;
           }
	        },
	        err => {
	          console.log("Error occured");
	          return err;
	        }
	  );
  }
  public changeStatus(status){
  	console.log(status)
  		this.manageJobsService.updateJobStatus(this.jobIdDelete,status)
	     .subscribe(
	        res => {
	          this.updatedJob=res.result;
            this.modal.hide();
	          this.ngOnInit();
             this.toastr.success('Deleted successfully')
	          return res;

	        },
	        err => {
	          console.log("Error occured");
	          return err;
	        }
	    );
  }
  open(content, modalName) {
  	console.log(modalName);
  	console.log("inside modal")
this.modelRef = this.modalService.open(content, { windowClass: ' org-modal', size: 'sm' });}

// getJobDetails(jobId){
// 	console.log(jobId);
// 	this.appService.getSingleJobDetails(jobId, JSON.parse(userId)).subscribe((data)=>{
// 		console.log(data);
// 	})
// 	}
}
