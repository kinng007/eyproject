var express = require("express")
var path=require('path'); 
var router = express.Router(); 
var user = require('../ey_module/controllers/user.js')



router.post('/first_signup',user.first_signup)
router.get('/account_verification/:email_id',user.account_verification)
router.get('/get_profile/:_id',user.get_profile)
router.post('/login',user.login)
router.put('/change_password/:_id',user.change_password)
router.put('/edit_profile/:_id',user.edit_profile)
router.post('/forget_password',user.forget_password)

module.exports = router;  
