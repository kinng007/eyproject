var express = require("express")
var path=require('path'); 
var router = express.Router(); 
var user = require('../ey_module/controllers/post.js')



router.post('/client_post',user.client_post)
router.get('/recruiter_view_job/:_id',user.recruiter_view_job)
router.get('/show_recuiter_dashbord_jobs',user.show_recuiter_dashbord_jobs)
router.put('/change_job_post_status/:post_id',user.change_job_post_status)
router.get('/getSkillSuggestion/:text',user.getSkillSuggestion)
router.get('/get_industry_list',user.get_industry_list)
router.post('/save_industry',user.save_industry)
router.post('/save_functionalArea',user.save_functionalArea)
router.get('/get_function_list',user.get_function_list)

router.put('/candidateBulkUpload/:job_id/:_id',user.candidateBulkUpload)
router.get('/client_managae_jobs/:_id',user.client_managae_jobs)
router.put('/edit_post/:_id',user.edit_post)
router.get('/recruiter_all_job_list',user.recruiter_all_job_list)
router.get('/consultant_manage_jobs/:_id',user.consultant_manage_jobs)
router.post('/bookmark_post',user.bookmark_post)
router.get('/recruiter_bookmark_job_list/:_id',user.recruiter_bookmark_job_list)
router.get('/client_dasboard_count/:_id',user.client_dasboard_count)
router.get('/recruiter_dasboard_count/:_id',user.recruiter_dasboard_count)
router.post('/client_shortlist_candidate',user.client_shortlist_candidate)
router.put('/upload_candidates_resume/:_id',user.upload_candidates_resume)
router.get('/recruiter_manage_candidate/:_id',user.recruiter_manage_candidate)
router.post('/withdraw_candidates',user.withdraw_candidates)
router.get('/client_dashboard_feed/:_id',user.client_dashboard_feed)
router.get('/client_view_resume/:_id/:job_id',user.client_view_resume)
router.get('/fetch_data/:_id/:job_id/:candidate_id',user.fetch_data)
router.get('/client_candidate_list/:_id',user.client_candidate_list)
router.get('/recruiter_company_calender_list/:_id',user.recruiter_company_calender_list)
router.get('/newGetSkillsuggestion',user.newGetSkillsuggestion)
router.get('/view_client_job_response/:_id:/:job_id',user.view_client_job_response)

router.get('/showjob/:_id',user.showjob)
router.get('/view_job_status_recruiter/:_id/:job_id',user.view_job_status_recruiter)
router.get('/view_client_job_response/:_id/:job_id',user.view_client_job_response)
router.get('/client_get_job_detail/:_id/:job_id',user.client_get_job_detail)

module.exports = router;  
