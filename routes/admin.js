var express = require("express")
var path=require('path'); 
var router = express.Router(); 
var admin = require('../ey_module/controllers/admin.js')


// router.get('/client_job_list/:_id',interview.client_job_list)
router.post('/admin_login',admin.admin_login)
router.get('/admin_full_job_list',admin.admin_full_job_list)
router.get('/admin_manage_user',admin.admin_manage_user)
router.get('/admin_latest_job_list',admin.admin_latest_job_list)
router.get('/admin_dasboard_count',admin.admin_dasboard_count)
router.get('/resdesk_admin',admin.resdesk_admin)
router.post('/admin_view_resume',admin.admin_view_resume)

router.post('/admin_cv_shortlist',admin.admin_cv_shortlist)

module.exports = router;  
