var express = require("express")
var path=require('path'); 
var router = express.Router(); 
var interview = require('../ey_module/controllers/interview.js')


router.get('/client_job_list/:_id',interview.client_job_list)
router.post('/save_time_slot_job',interview.save_time_slot_job)
router.get('/recruiter_interview_dashboard/:_id',interview.recruiter_interview_dashboard)
router.get('/client_interview_dashboard/:_id',interview.client_interview_dashboard)
router.get('/client_slot_aviable_list/:_id',interview.client_slot_aviable_list)
router.get('/recruiter_calender_list/:_id/:job_id',interview.recruiter_calender_list)


module.exports = router;  
